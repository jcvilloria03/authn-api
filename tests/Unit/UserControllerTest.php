<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Event;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test the createUser method.
     */
    public function testCreateUser()
    {
        // Fake the Registered event
        Event::fake();

        // Create a request data
        $data = [
            'email' => 'test@example.com',
            'first_name' => 'Test',
            'last_name' => 'User',
            'password' => 'password123',
            'plan_id' => '1',
            'name' => 'Test User',
            'company' => 'Test Company',
            'metadata' => [
                'account_id' => 1,
                'account_name' => 'Test Account',
                'company_id' => 2,
                'company_name' => 'Test Company',
                'user_id' => 3,
                'employee_id' => 4,
                'role_id' => 5,
                'role_name' => 'Test Role',
                'initial_password' => 'password123',
            ],
            'status' => 'new',
        ];

        // Make a post request
        $response = $this->json('POST', '/api/v1/users', $data);

        // Assert the response
        $response->assertStatus(201);
        $response->assertJson([
            'data' => [
                'email' => 'test@example.com',
                'first_name' => 'Test',
                'last_name' => 'User',
            ]
        ]);

        // Assert the user was created in the database
        $this->assertDatabaseHas('users', [
            'email' => 'test@example.com',
            'first_name' => 'Test',
            'last_name' => 'User',
        ]);

        // Assert that the Registered event was dispatched
        Event::assertDispatched(Registered::class);
    }

    /**
     * Test the createUser method with invalid data.
     */
    public function testCreateUserWithInvalidData()
    {
        // Fake the Registered event
        Event::fake();

        // Create a request with invalid data
        $invalidData = [
            'email' => 'not an email',
            'first_name' => '',
            'last_name' => '',
            'status' => 'not in status list',
            'password' => 'short',
            'plan_id' => 'not a number',
            // Add more fields as necessary
        ];

        // Make a post request
        $response = $this->json('POST', '/api/v1/users', $invalidData);

        // Assert the response
        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['email', 'first_name', 'last_name', 'status', 'metadata']);

        // Assert that the Registered event was not dispatched
        Event::assertNotDispatched(Registered::class);
    }

    /**
     * Test the getUsers method.
     */
    public function testGetUsers()
    {
        $user1 = User::create([
            'first_name' => 'User',
            'last_name' => 'One',
            'email' => 'user1@example.com',
            'password' => Hash::make('password')
        ]);

        $user2 = User::create([
            'first_name' => 'User',
            'last_name' => 'Two',
            'email' => 'user2@example.com',
            'password' => Hash::make('password')
        ]);

        // Hit the endpoint
        $response = $this->get('/api/v1/users');

        // Assert that the users are in the response
        $response->assertStatus(200)
            ->assertJsonPath('data.0.email', 'user1@example.com')
            ->assertJsonPath('data.0.id', $user1->id)
            ->assertJsonPath('data.1.email', 'user2@example.com')
            ->assertJsonPath('data.1.id', $user2->id);
    }

    /**
     * Test the updateUser method.
     */
    public function testUpdateUser()
    {
        // Create a user
        $user = User::create([
            'email' => 'test@example.com',
            'first_name' => 'Test',
            'last_name' => 'User',
            'password' => Hash::make('password123'),
            'status' => 'new',
        ]);

        // Create a request data
        $data = [
            'email' => 'updated@example.com',
            'first_name' => 'Updated',
            'last_name' => 'User',
            'status' => 'active',
            'metadata' => [
                'account_id' => 1,
                'account_name' => 'Updated Account',
                'company_id' => 2,
                'company_name' => 'Updated Company',
                'user_id' => 3,
                'employee_id' => 4,
                'role_id' => 5,
                'role_name' => 'Updated Role',
            ],
        ];

        // Make a put request
        $response = $this->json('PUT', "/api/v1/users/{$user->id}", $data);

        // Assert the response
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'email' => 'updated@example.com',
                'first_name' => 'Updated',
                'last_name' => 'User',
            ]
        ]);

        // Assert the user was updated in the database
        $this->assertDatabaseHas('users', [
            'email' => 'updated@example.com',
            'first_name' => 'Updated',
            'last_name' => 'User',
        ]);
    }

    /**
     * Test the updateUser method with invalid data.
     */
    public function testUpdateUserWithInvalidData()
    {
        // Create a user
        $user = User::create([
            'email' => 'test@example.com',
            'first_name' => 'Test',
            'last_name' => 'User',
            'password' => Hash::make('password123'),
            'status' => 'new',
        ]);

        // Create invalid request data
        $data = [
            'email' => 'not an email',
            'first_name' => '',
            'last_name' => '',
            'status' => 'not a status',
            'metadata' => 'not an object',
        ];

        // Make a put request
        $response = $this->json('PUT', "/api/v1/users/{$user->id}", $data);

        // Assert the response
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => [
                'email',
                'first_name',
                'last_name',
                'status',
                'metadata',
            ],
        ]);
    }

    public function testUpdateNonExistentUser()
    {
        // Create invalid user ID
        $invalidUserId = 9999; // Assuming this ID does not exist in your database

        // Create valid request data
        $data = [
            'email' => 'test@example.com',
            'first_name' => 'Test',
            'last_name' => 'User',
            'status' => 'active',
        ];

        // Make a put request
        $response = $this->json('PUT', "/api/v1/users/{$invalidUserId}", $data);

        // Assert the response
        $response->assertStatus(404);
    }

    /**
     * Test the getUserById method.
     */
    public function testGetUsersById()
    {
        $user = User::create([
            'first_name' => 'User',
            'last_name' => 'One',
            'email' => 'user1@example.com',
            'password' => Hash::make('password')
        ]);

        // Hit the endpoint
        $response = $this->get('/api/v1/users/' . $user->id);

        // Assert that the users are in the response
        $response->assertStatus(200)
            ->assertJsonPath('data.email', 'user1@example.com')
            ->assertJsonPath('data.id', $user->id);
    }

    /**
     * Test the deleteUser method.
     */
    public function testDeleteUser()
    {
        $user = User::create([
            'first_name' => 'User',
            'last_name' => 'One',
            'email' => 'user1@example.com',
            'password' => Hash::make('password')
        ]);

        // Hit the endpoint
        $response = $this->delete('/api/v1/users/' . $user->id);

        // Assert that the users are in the response
        $response->assertStatus(204);

        // Assert the user was soft deleted in the database
        $this->assertSoftDeleted('users', [
            'email' => 'user1@example.com',
            'first_name' => 'User',
            'last_name' => 'One',
        ]);
    }

    /**
     * Test the resendVerifyEmail method.
     */
    public function testResendVerifyEmail()
    {
        $user = User::create([
            'first_name' => 'User',
            'last_name' => 'One',
            'email' => 'user1@example.com',
            'password' => Hash::make('password'),
            'metadata' => [
                'name' => "User One",
                'company' => 'Test Company',
                'plan_id' => 3,
                'initial_password' => Hash::make('password')
            ]
        ]);

        // Hit the endpoint
        $response = $this->post('/api/v1/auth/email/verify/' . $user->id . '/resend');

        // Assert the response
        $response->assertStatus(302);
    }

    /**
     * Test the forgotPassword method.
     */
    public function testForgotPassword()
    {
        User::create([
            'first_name' => 'User',
            'last_name' => 'One',
            'email' => 'user1@example.com',
            'password' => Hash::make('password'),
            'metadata' => [
                'name' => "User One",
                'company' => 'Test Company',
                'plan_id' => 3,
                'initial_password' => Hash::make('password')
            ]
        ]);

        // Create a request data
        $data = [
            'email' => 'user1@example.com',
        ];

        // Make a post request
        $response = $this->json('POST', '/api/v1/auth/forgot-password', $data);

        // Assert the response
        $response->assertStatus(200);
    }

    /**
     * Test the changePassword method.
     */
    public function testChangePassword()
    {
        User::create([
            'first_name' => 'User',
            'last_name' => 'One',
            'email' => 'user1@example.com',
            'password' => Hash::make('password'),
            'metadata' => [
                'name' => "User One",
                'company' => 'Test Company',
                'plan_id' => 3,
                'initial_password' => Hash::make('password')
            ]
        ]);

        // Create a request data
        $data = [
            'email' => 'user1@example.com',
            'old_password' => 'password',
            'new_password' => 'newpassword',
            'password_confirmation' => 'newpassword',
        ];

        // Make a post request
        $response = $this->json('POST', '/api/v1/auth/change-password', $data);

        // Assert the response
        $response->assertStatus(200);
    }
}