<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ValidationControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test the validateCsv method with a valid CSV file.
     *
     * @return void
     */
    public function testValidateCsvWithValidData()
    {
        // Create a fake CSV file with valid data
        $file = UploadedFile::fake()->createWithContent(
            'document.csv',
            "first_name,last_name,email,password,status\nJohn,Doe,john@example.com,secret,new"
        );

        // Make a POST request to the validateCsv route
        $response = $this->postJson('/api/v1/users/batch/validation', [
            'csv' => $file,
            'type' => 'create',
        ]);

        // Assert that the response status is 200 (OK)
        $response->assertStatus(200);

        // Assert that the response has the expected structure
        $response->assertJsonStructure([
            'status',
            'message',
        ]);
    }

    /**
     * Test the validateCsv method with an csv file with invalid data.
     *
     * @return void
     */
    public function testValidateCsvWithInvalidData()
    {
        // Create a fake CSV file with invalid data
        $file = UploadedFile::fake()->createWithContent(
            'document.csv',
            "first_name,last_name,email,password,status\nJohn,Doe,not_an_email,secret,not_a_status"
        );

        // Make a POST request to the validateCsv route
        $response = $this->postJson('/api/v1/users/batch/validation', [
            'csv' => $file,
            'type' => 'create',
        ]);

        // Assert that the response status is 422 (Unprocessable Entity)
        $response->assertStatus(422);

        // Assert that the response has the expected error message
        $response->assertJson([
            'status' => 'error',
            'errors' => [
                "1" => [
                    'email' => [
                        'The email field must be a valid email address.',
                    ],
                    'status' => [
                        'The selected status is invalid.',
                    ]
                ],
            ],
        ]);
    }

    /**
     * Test the validateCsv method with an empty CSV file.
     *
     * @return void
     */
    public function testValidateCsvWithEmptyFile()
    {
        // Create a fake CSV file with only a header row
        $file = UploadedFile::fake()->createWithContent('document.csv', "first_name,last_name,email,password");


        // Make a POST request to the validateCsv route
        $response = $this->postJson('/api/v1/users/batch/validation', [
            'csv' => $file,
            'type' => 'create',
        ]);

        // Assert that the response status is 422 (Unprocessable Entity)
        $response->assertStatus(422);

        // Assert that the response has the expected error message
        $response->assertJson([
            'status' => 'error',
            'message' => 'The CSV file is empty.',
        ]);
    }

    /**
     * Test the validateCsv method with missing required columns.
     *
     * @return void
     */
    public function testValidateCsvWithMissingColumns()
    {
        // Create a fake CSV file with missing required columns
        $file = UploadedFile::fake()->createWithContent('document.csv', "missing,columns\nvalue1,value2");

        // Make a POST request to the validateCsv route
        $response = $this->postJson('/api/v1/users/batch/validation', [
            'csv' => $file,
            'type' => 'create',
        ]);

        // Assert that the response status is 422 (Unprocessable Entity)
        $response->assertStatus(422);

        // Assert that the response has the expected error message
        $response->assertJson([
            'status' => 'error',
            'message' => 'Missing columns: email, first_name, last_name, password, status',
        ]);
    }

    public function testValidateCsvWithValidDataForUpdate()
    {
        // Create a mock user
        $user = User::create([
            'email' => 'test@example.com',
            'first_name' => 'Test',
            'last_name' => 'User',
            'password' => Hash::make('password123'),
            'status' => 'new',
        ]);

        // Create a fake CSV file with valid data
        $file = UploadedFile::fake()->createWithContent(
            'document.csv',
            "authn_user_id,first_name,last_name,email,status\n{$user->id},John,Doe,john@example.com,new"
        );

        // Make a POST request to the validateCsv route
        $response = $this->postJson('/api/v1/users/batch/validation', [
            'csv' => $file,
            'type' => 'update',
        ]);

        // Assert that the response status is 200 (OK)
        $response->assertStatus(200);

        // Assert that the response has the expected structure
        $response->assertJsonStructure([
            'status',
            'message',
        ]);
    }

    public function testValidateCsvWithInvalidDataForUpdate()
    {
        // Create a fake CSV file with invalid data
        $file = UploadedFile::fake()->createWithContent(
            'document.csv',
            "authn_user_id,first_name,last_name,email,status\nnot_a_number,John,Doe,not_an_email,not_a_status"
        );

        // Make a POST request to the validateCsv route
        $response = $this->postJson('/api/v1/users/batch/validation', [
            'csv' => $file,
            'type' => 'update',
        ]);

        // Assert that the response status is 422 (Unprocessable Entity)
        $response->assertStatus(422);

        // Assert that the response has the expected error message
        $response->assertJson([
            'status' => 'error',
            'errors' => [
                "1" => [
                    'authn_user_id' => [
                        'The authn user id field must be an integer.',
                    ],
                    'email' => [
                        'The email field must be a valid email address.',
                    ],
                    'status' => [
                        'The selected status is invalid.',
                    ]
                ],
            ],
        ]);
    }

    public function testValidateCsvWithMissingColumnsForUpdate()
    {
        // Create a fake CSV file with missing required columns
        $file = UploadedFile::fake()->createWithContent('document.csv', "missing,columns\nvalue1,value2");

        // Make a POST request to the validateCsv route
        $response = $this->postJson('/api/v1/users/batch/validation', [
            'csv' => $file,
            'type' => 'update',
        ]);

        // Assert that the response status is 422 (Unprocessable Entity)
        $response->assertStatus(422);

        // Assert that the response has the expected error message
        $response->assertJson([
            'status' => 'error',
            'message' => 'Missing columns: authn_user_id, email, first_name, last_name, status',
        ]);
    }
}