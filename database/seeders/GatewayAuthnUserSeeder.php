<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GatewayAuthnUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->command->info('GW-API authn_users Seeder start.');

        $databasePrefix = env('DB_PREFIX', '');

        $gwAuthnUsersTable = $databasePrefix.'gwapi.authn_users';
        $userTable = $databasePrefix.'cpapi.users';
        $companiesUsersTable = $databasePrefix.'cpapi.companies_users';
        $authnUsersTable = $databasePrefix.'authnapi.users';

        $users = DB::table($userTable)
            ->join($companiesUsersTable, $companiesUsersTable.'.user_id', '=', $userTable.'.id')
            ->join($authnUsersTable, $authnUsersTable.'.email', '=', $userTable.'.email')
            ->select($companiesUsersTable.'.user_id', $userTable.'.account_id', $authnUsersTable.'.id as authn_user_id',
                $companiesUsersTable.'.company_id', $companiesUsersTable.'.employee_id', $userTable.'.status')
            ->orderBy($companiesUsersTable.'.employee_id', 'desc')
            ->get()
            ->toArray();

        $usersData = [];
        foreach ($users as $user) {
            $exist = collect($usersData)->where('authn_user_id', $user->authn_user_id)->first();
            if (!$exist) {
                $usersData[] = [
                    "user_id" => $user->user_id,
                    "account_id" => $user->account_id,
                    "authn_user_id" => $user->authn_user_id,
                    "company_id" => $user->company_id,
                    "employee_id" => $user->employee_id,
                    "status" => $user->status
                ];
            }
        }

        $insertedTotal = 0;
        $this->command->info('Total users to inserted: ' . count($usersData));

        $chunkedUsersData = array_chunk($usersData, 1000);

        DB::beginTransaction();
        try {
            foreach ($chunkedUsersData as $userDataChunk) {
                $result = DB::table($gwAuthnUsersTable)->insertOrIgnore($userDataChunk);
                $insertedTotal = $insertedTotal+$result;
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            throw $e;
        }

        $this->command->info('Total Inserted Users: ' . $insertedTotal);
        $this->command->info('GW-API authn_users Seeder done.');
    }
}
