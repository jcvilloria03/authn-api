<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Auth0LoginDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new Client(['base_uri' => env('AUTH0_DOMAIN')]);
        $accessToken = $this->getAccessToken($client);
        $connectionId = $this->getConnectionId($client, $accessToken);
        $jobId = $this->getJobId($client, $accessToken, $connectionId);
        $fileUrl = $this->getFileUrl($client, $accessToken, $jobId);
        $this->processFile($fileUrl);
    }

    /**
     * Extracts a gzipped CSV file.
     *
     * @param  string  $gzFilePath Path to the gzipped file.
     * @param  string  $extractedFilePath Path where the extracted file should be saved.
     */
    protected function extractCsv($gzFilePath, $extractedFilePath)
    {
        $bufferSize = 4096; // read 4kb at a time
        $outFile = gzopen($gzFilePath, 'rb');
        $inFile = fopen($extractedFilePath, 'wb');

        while (! gzeof($outFile)) {
            fwrite($inFile, gzread($outFile, $bufferSize));
        }

        fclose($inFile);
        gzclose($outFile);
    }

    /**
     * Retrieves an access token from Auth0.
     *
     * @param  GuzzleHttp\Client  $client The HTTP client instance.
     * @return string The access token.
     */
    private function getAccessToken($client)
    {
        $response = $client->post('/oauth/token', [
            'json' => [
                'client_id' => env('AUTH0_CLIENT_ID'),
                'client_secret' => env('AUTH0_CLIENT_SECRET'),
                'audience' => env('AUTH0_DOMAIN').'/api/v2/',
                'grant_type' => 'client_credentials',
            ],
        ]);

        return json_decode($response->getBody())->access_token;
    }

    /**
     * Retrieves an access token from Auth0.
     *
     * @param  GuzzleHttp\Client  $client The HTTP client instance.
     * @return string The access token.
     */
    private function getConnectionId($client, $accessToken)
    {
        $response = $client->get('/api/v2/connections', [
            'headers' => [
                'Authorization' => 'Bearer '.$accessToken,
            ],
        ]);

        $connections = json_decode($response->getBody());
        $connectionId = null;

        foreach ($connections as $connection) {
            if ($connection->name === 'Username-Password-Authentication') {
                $connectionId = $connection->id;
                break;
            }
        }

        if (! $connectionId) {
            throw new Exception('No available Username-Password-Authentication connection');
        }

        return $connectionId;
    }

    /**
     * Initiates a job in Auth0 to export users data.
     *
     * @param  GuzzleHttp\Client  $client The HTTP client instance.
     * @param  string  $accessToken The access token.
     * @param  string  $connectionId The connection ID.
     * @return string The job ID.
     */
    private function getJobId($client, $accessToken, $connectionId)
    {
        $fields = [
            ['name' => 'created_at'],
            ['name' => 'email'],
            ['name' => 'email_verified'],
            ['name' => 'name'],
            ['name' => 'updated_at'],
            ['name' => 'user_id'],
            ['name' => 'user_metadata'],
            ['name' => 'last_ip'],
            ['name' => 'last_login'],
            ['name' => 'logins_count'],
            ['name' => 'migrated'],
        ];

        $response = $client->post('/api/v2/jobs/users-exports', [
            'json' => [
                'connection_id' => $connectionId,
                'format' => 'json',
                'fields' => $fields,
            ],
            'headers' => [
                'Authorization' => 'Bearer '.$accessToken,
                'Content-Type' => 'application/json',
            ],
        ]);

        return json_decode($response->getBody())->id;
    }

    /**
     * Retrieves the URL of the file containing the exported users data from Auth0.
     *
     * @param  GuzzleHttp\Client  $client The HTTP client instance.
     * @param  string  $accessToken The access token.
     * @param  string  $jobId The job ID.
     * @return string The URL of the file.
     *
     * @throws Exception If the job does not complete after 20 retries.
     */
    private function getFileUrl($client, $accessToken, $jobId)
    {
        $retries = 0;
        $fileUrl = null;

        while ($retries <= 20) {
            $response = $client->get("/api/v2/jobs/{$jobId}", [
                'headers' => [
                    'Authorization' => 'Bearer '.$accessToken,
                ],
            ]);

            $jobResponse = json_decode($response->getBody(), true);

            if ($jobResponse['status'] === 'completed') {
                $fileUrl = $jobResponse['location'];
                break;
            }

            $retries++;
            sleep(5); // wait for 5 seconds before the next retry
        }

        if (! $fileUrl) {
            throw new Exception('Job did not complete after 20 retries');
        }

        return $fileUrl;
    }

    /**
     * Processes the file containing the exported users data from Auth0.
     *
     * @param  string  $fileUrl The URL of the file.
     *
     * @throws Exception If the file cannot be read.
     */
    private function processFile($fileUrl)
    {
        $filename = 'auth0-users:'.Carbon::now()->format('Y-m-d').':'.rand(1, 1000);
        $filePath = storage_path("app/{$filename}.gz");

        // Download file
        file_put_contents($filePath, file_get_contents($fileUrl));

        // Extract from gz
        $csvFilePath = storage_path("app/{$filename}.json");
        $this->extractCsv($filePath, $csvFilePath);

        // Then, read the file and insert the data into the database
        $data = file_get_contents($csvFilePath);
        if ($data === false || is_null($data)) {
            throw new Exception('Failed to read data from file');
        }

        $users = array_map('json_decode', explode("\n", trim($data)));
        $users = array_filter($users, function ($user) {
            return ! is_null($user);
        });

        $logged_in_emails = array_column($users, 'email');

        $databasePrefix = env('DB_PREFIX', '');

        $userTable = $databasePrefix.'cpapi.users';
        $companiesUsersTable = $databasePrefix.'cpapi.companies_users';
        $companiesTable = $databasePrefix.'cpapi.companies';
        $employeesTable = $databasePrefix.'cpapi.employees';
        $accountsTable = $databasePrefix.'cpapi.accounts';
        $rolesTable = $databasePrefix.'cpapi.roles';

        $usersInfo = DB::table($userTable)
            ->leftJoin($companiesUsersTable, $companiesUsersTable.'.user_id', '=', $userTable.'.id')
            ->leftJoin($companiesTable, $companiesTable.'.id', '=', $companiesUsersTable.'.company_id')
            ->leftJoin($employeesTable, $employeesTable.'.id', '=', $companiesUsersTable.'.employee_id')
            ->leftJoin($accountsTable, $accountsTable.'.id', '=', $companiesTable.'.account_id')
            ->leftJoin($rolesTable, $rolesTable.'.id', '=', $userTable.'.role_id')
            ->whereIn($userTable.'.email', $logged_in_emails)
            ->select($userTable.'.email', $userTable.'.account_id', $companiesUsersTable.'.company_id',
                $userTable.'.id as user_id', $companiesUsersTable.'.employee_id',
                $companiesTable.'.name as company_name', $accountsTable.'.name as account_name',
                $userTable.'.role_id', $rolesTable.'.name as role_name', $userTable.'.user_type',
                $userTable.'.first_name as v3_first_name', $userTable.'.last_name as v3_last_name',
                $userTable.'.middle_name as v3_middle_name',
                $employeesTable.'.mobile_number as v3_mobile_number', )
            ->get()
            ->keyBy('email')
            ->toArray();

        $this->insertUsersIntoDatabase($users, $usersInfo);
    }

    /**
     * Inserts user data into the database in chunks.
     *
     * @param  array  $users The array of user data.
     * @param  array  $usersInfo Additional user information.
     *
     * @throws Exception If there is an error during the database transaction.
     */
    private function insertUsersIntoDatabase($users, $usersInfo = [])
    {
        $usersData = [];
        foreach ($users as $user) {
            // Merge usersInfo into user_metadata
            if (isset($usersInfo[$user->email])) {
                $user->user_metadata = array_merge((array) $user->user_metadata, (array) $usersInfo[$user->email]);
                $usersData[] = $this->prepareUserData($user);
            }
        }

        $chunkedUsersData = array_chunk($usersData, 1000);

        DB::beginTransaction();
        try {
            foreach ($chunkedUsersData as $userDataChunk) {
                DB::table('users')->insert($userDataChunk);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * Prepares user data for insertion into the database.
     *
     * @param  object  $user The user object.
     * @return array The prepared user data.
     */
    private function prepareUserData($user)
    {
        if (! property_exists($user, 'user_metadata')) {
            $user->user_metadata = [];
        }
        if (is_object($user->user_metadata)) {
            $user->user_metadata = (array) $user->user_metadata;
        }
        $fresh_account = $user->user_metadata ? $user->user_metadata['fresh_account'] : false;
        $email_verified = $user->email_verified ?? false;
        $status = $fresh_account && ! $email_verified ? User::STATUS_NEW : User::STATUS_ACTIVE;

        $firstName = current(array_filter([
            $user->user_metadata['first_name'] ?? null,
            $user->user_metadata['v3_first_name'] ?? null,
            $user->name ?? null,
        ]));

        $lastName = current(array_filter([
            $user->user_metadata['last_name'] ?? null,
            $user->user_metadata['v3_last_name'] ?? null,
            $user->name ?? null,
        ]));

        $middleName = current(array_filter([
            $user->user_metadata['middle_name'] ?? null,
            $user->user_metadata['v3_middle_name'] ?? null,
            $user->name ?? null,
        ]));

        $mobileNumber = $user->user_metadata['mobile_number']
            ?? $user->user_metadata['v3_mobile_number']
            ?? null;

        if (! property_exists($user, 'last_login')) {
            $user->last_login = null;
        }

        $metadataKeys = [
            'account_id', 'account_name', 'company_id', 'company_name',
            'user_id', 'employee_id', 'role_id', 'role_name', 'user_type',
        ];

        $metadata = array_intersect_key($user->user_metadata, array_flip($metadataKeys));

        return [
            'email' => $user->email,
            'password' => null, // Password is not available from Auth0 data
            'first_name' => $firstName,
            'last_name' => $lastName,
            'middle_name' => $middleName,
            'mobile_number' => $mobileNumber,
            'status' => $status,
            'metadata' => json_encode($metadata),
            'login_count' => $user->logins_count ?? 0,
            'last_login_at' => $user->last_login ? Carbon::parse($user->last_login)->format('Y-m-d H:i:s') : null,
            'email_verified_at' => $user->email_verified ? Carbon::now() : null,
            'created_at' => $user->created_at ? Carbon::parse($user->created_at)->format('Y-m-d H:i:s') : null,
            'updated_at' => $user->updated_at ? Carbon::parse($user->updated_at)->format('Y-m-d H:i:s') : null,
            'deleted_at' => null,
        ];
    }
}
