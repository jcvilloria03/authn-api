<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ClientSeeder extends Seeder
{
    use WithoutModelEvents;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $microservices = ['GW-API', 'CP-API'];

        foreach ($microservices as $item) {
            DB::table('oauth_clients')->insert([
                'id' => Str::orderedUuid(),
                'name' => $item,
                'secret' => Str::random(42),
                'redirect' => 'http://' . strtolower($item),
                'personal_access_client' => 0,
                'password_client' => 0,
                'provider' => 'users',
                'revoked' => 0,
            ]);
        }
    }
}
