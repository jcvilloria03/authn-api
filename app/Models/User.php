<?php

namespace App\Models;

use App\Notifications\ResetPasswordEmailNotification;
use App\Notifications\VerifyEmailNotification;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements CanResetPassword, MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    const STATUS_NEW = 'new'; // newly created

    const STATUS_PENDING = 'pending'; // email is verified but salarium account creation in progress

    const STATUS_ACTIVE = 'active'; // email is verified and account is already created

    const STATUS_INACTIVE = 'inactive'; // inactive or deactivated user

    const STATUSES = [
        self::STATUS_NEW,
        self::STATUS_PENDING,
        self::STATUS_ACTIVE,
        self::STATUS_INACTIVE,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'middle_name',
        'mobile_number',
        'status',
        'metadata',
        'login_count',
        'last_login_at',
        'email_verified_at',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'last_login_at' => 'datetime',
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
        'metadata' => 'json',
    ];

    public function sendEmailVerificationNotification()
    {
        $url = URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(config('authn.verification.expire', 7200)),
            [
                'id' => $this->getKey(),
                'hash' => sha1($this->getEmailForVerification()),
            ],
            false
        );
        $verifyUrl = config('authn.gw_api.url') . $url;

        $template = $this->isFromSignup() ? 'emails.signup_verification' : 'emails.user_verification';

        $this->notify(new VerifyEmailNotification($verifyUrl, $template, $this));
    }

    /**
     * Sends the reset password email notification
     *
     * @param  mixed  $token
     */
    public function sendPasswordResetNotification($token): void
    {
        $url = URL::temporarySignedRoute(
            'password.reset',
            Carbon::now()->addMinutes(60),
            [
                'token' => $token,
                'email' => $this->email,
            ],
            false
        );
        $originalUrl = route('password.reset', [], false);
        $frontEndUrl = config('authn.pr_client.url.reset_password');

        $resetUrl = str_replace($originalUrl, $frontEndUrl, $url);

        $this->notify(new ResetPasswordEmailNotification($resetUrl));
    }

    public function isFromSignup()
    {
        return Arr::get($this->metadata, 'is_signup', false);
    }

    public function companyUserId()
    {
        return Arr::get($this->metadata, 'user_id', null);
    }
}
