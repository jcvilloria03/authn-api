<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="UserExtended",
 *     description="Extended User model",
 *     @OA\Xml(
 *         name="UserExtended"
 *     )
 * )
 */
class UserExtended extends User
{
    /**
     * @OA\Property(
     *      title="Metadata",
     *      description="Additional data of the new User",
     *      type="object",
     *      @OA\Property(
     *          property="account_id",
     *          description="Account ID of the new user",
     *          @OA\Schema(type="integer"),
     *          example=1
     *      ),
     *      @OA\Property(
     *          property="account_name",
     *          description="Account name of the new user",
     *          @OA\Schema(type="string"),
     *          example="Test Account"
     *      ),
     *      @OA\Property(
     *          property="company_id",
     *          description="Company ID of the new user",
     *          @OA\Schema(type="integer"),
     *          example=2
     *      ),
     *      @OA\Property(
     *          property="company_name",
     *          description="Company name of the new user",
     *          @OA\Schema(type="string"),
     *          example="Test Company"
     *      ),
     *      @OA\Property(
     *          property="user_id",
     *          description="User ID of the new user",
     *          @OA\Schema(type="integer"),
     *          example=3
     *      ),
     *      @OA\Property(
     *          property="employee_id",
     *          description="Employee ID of the new user",
     *          @OA\Schema(type="integer"),
     *          example=4
     *      ),
     *      @OA\Property(
     *          property="role_id",
     *          description="Role ID of the new user",
     *          @OA\Schema(type="integer"),
     *          example=5
     *      ),
     *      @OA\Property(
     *          property="role_name",
     *          description="Role name of the new user",
     *          @OA\Schema(type="string"),
     *          example="Test Role"
     *      ),
     *      @OA\Property(
     *          property="initial_password",
     *          description="Initial password of the new user",
     *          @OA\Schema(type="string"),
     *          example="password123"
     *      ),
     * )
     *
     * @var object
     */
    public $metadata;
}