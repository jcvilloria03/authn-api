<?php

namespace App\Virtual\Models;


/**
 * @OA\Schema(
 *     title="User",
 *     description="User model",
 *     @OA\Xml(
 *         name="User"
 *     )
 * )
 */
class User
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Email",
     *      description="Email of the new User",
     *      format="email",
     *      example="user1@email.com"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *      title="First Name",
     *      description="First name of the new User",
     *      example="Jonie"
     * )
     *
     * @var string
     */
    public $first_name;

    /**
     * @OA\Property(
     *      title="Last Name",
     *      description="Last name of the new User",
     *      example="Dela Cruz"
     * )
     *
     * @var string
     */
    public $last_name;

    /**
     * @OA\Property(
     *      title="Middle Name",
     *      description="Middle name of the new User",
     *      example="Doe"
     * )
     *
     * @var string
     */
    public $middle_name;

    /**
     * @OA\Property(
     *      title="Mobile Number",
     *      description="Mobile number of the new User",
     *      example="9700000000"
     * )
     *
     * @var string
     */
    public $mobile_number;

    /**
     * @OA\Property(
     *      title="Status",
     *      description="Status of the new User",
     *      example="new"
     * )
     *
     * @var string
     */
    public $status;

    /**
     * @OA\Property(
     *      title="Metadata",
     *      description="Additional data of the new User",
     *      type="object",
     *      @OA\Property(
     *          property="name",
     *          description="Account name of the new user",
     *          @OA\Schema(type="string"),
     *          example="Jonie Dela Cruz Company"
     *      ),
     *      @OA\Property(
     *          property="company",
     *          description="Company name of the new user",
     *          @OA\Schema(type="string"),
     *          example="Jonie Dela Cruz Company"
     *      ),
     *      @OA\Property(
     *          property="plan_id",
     *          description="Plan ID of the new User",
     *          @OA\Schema(type="integer"),
     *          example=3
     *      ),
     * )
     *
     * @var object
     */
    public $metadata;

    /**
     * @OA\Property(
     *     title="Login Count",
     *     description="User login count",
     *     example=0,
     *     type="integer"
     * )
     *
     * @var int
     */
    private $login_count;

    /**
     * @OA\Property(
     *     title="Last Login at",
     *     description="User's last login datetime",
     *     example=null,
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    private $last_login_at;

    /**
     * @OA\Property(
     *     title="Created at",
     *     description="Created at",
     *     example="2023-11-27 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    private $created_at;

    /**
     * @OA\Property(
     *     title="Updated at",
     *     description="Updated at",
     *     example="2023-11-27 17:50:45",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    private $updated_at;
}