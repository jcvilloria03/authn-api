<?php

namespace App\Virtual;

/**
 * @OA\Schema(
 *      title="Change Password request",
 *      description="Change Password request body data",
 *      type="object",
 *      required={"email","old_password","new_password"}
 * )
 */

class ChangePasswordRequest
{
    /**
     * @OA\Property(
     *      title="Email",
     *      description="Email of the User",
     *      format="email",
     *      example="user1@email.com"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *      title="Current Password",
     *      description="Current password of the user",
     *      example="password"
     * )
     *
     * @var string
     */
    public $old_password;

    /**
     * @OA\Property(
     *      title="New Password",
     *      description="New password of the user",
     *      example="newpassword"
     * )
     *
     * @var string
     */
    public $new_password;

    /**
     * @OA\Property(
     *      title="New Password Confirmation",
     *      description="New password confirmation of the user",
     *      example="newpassword"
     * )
     *
     * @var string
     */
    public $password_confirmation;
}