<?php

namespace App\Virtual;

/**
 * @OA\Schema(
 *      title="Signup Account request",
 *      description="Signup User request body data",
 *      type="object",
 *      required={"name","email","first_name","last_name","company","plan_id","password"}
 * )
 */

class SignupUserRequest
{
    /**
     * @OA\Property(
     *      title="Name",
     *      description="Account name of the new user",
     *      type="string",
     *      example="Jonie Dela Cruz Company"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="Email",
     *      description="Email of the new User",
     *      format="email",
     *      example="user1@email.com"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *      title="First Name",
     *      description="First name of the new User",
     *      example="Jonie"
     * )
     *
     * @var string
     */
    public $first_name;

    /**
     * @OA\Property(
     *      title="Last Name",
     *      description="Last name of the new User",
     *      example="Dela Cruz"
     * )
     *
     * @var string
     */
    public $last_name;

    /**
     * @OA\Property(
     *      title="Company",
     *      description="Company name of the new User",
     *      example="Jonie Dela Cruz Company"
     * )
     *
     * @var string
     */
    public $company;

    /**
     * @OA\Property(
     *      title="Plan ID",
     *      description="Plan ID of the new User",
     *      example=3
     * )
     *
     * @var int
     */
    public $plan_id;

    /**
     * @OA\Property(
     *      title="Password",
     *      description="Password of the new User",
     *      example="password@#1234"
     * )
     *
     * @var string
     */
    public $password;
}