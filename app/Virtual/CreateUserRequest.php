<?php

namespace App\Virtual;

/**
 * @OA\Schema(
 *      title="Create User request",
 *      description="Create User request body data",
 *      type="object",
 *      required={"email","password","first_name","last_name","status","metadata"}
 * )
 */

class CreateUserRequest
{
    /**
     * @OA\Property(
     *      title="Email",
     *      description="User's email",
     *      format="email",
     *      example="user1@example.com"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *      title="Password",
     *      description="User's password",
     *      example="password@#1234"
     * )
     *
     * @var string
     */
    public $password;

    /**
     * @OA\Property(
     *      title="First Name",
     *      description="User's first name",
     *      example="John"
     * )
     *
     * @var string
     */
    public $first_name;

    /**
     * @OA\Property(
     *      title="Last Name",
     *      description="User's last name",
     *      example="Doe"
     * )
     *
     * @var string
     */
    public $last_name;

    /**
     * @OA\Property(
     *      title="Middle Name",
     *      description="User's middle name",
     *      example="Middle"
     * )
     *
     * @var string
     */
    public $middle_name;

    /**
     * @OA\Property(
     *      title="Mobile Number",
     *      description="User's mobile number",
     *      example="1234567890"
     * )
     *
     * @var string
     */
    public $mobile_number;

    /**
     * @OA\Property(
     *      title="Status",
     *      description="User's status",
     *      example="active"
     * )
     *
     * @var string
     */
    public $status;

    /**
     * @OA\Property(
     *      title="Metadata",
     *      description="User's metadata",
     *      type="object",
     *      required={"account_id", "account_name", "user_id", "role_id", "role_name", "initial_password"},
     *      @OA\Property(
     *          property="account_id",
     *          type="integer",
     *          example=1
     *      ),
     *      @OA\Property(
     *          property="account_name",
     *          type="string",
     *          example="Test Account"
     *      ),
     *      @OA\Property(
     *          property="company_id",
     *          type="integer",
     *          example=2
     *      ),
     *      @OA\Property(
     *          property="company_name",
     *          type="string",
     *          example="Test Company"
     *      ),
     *      @OA\Property(
     *          property="user_id",
     *          type="integer",
     *          example=3
     *      ),
     *      @OA\Property(
     *          property="employee_id",
     *          type="integer",
     *          example=4
     *      ),
     *      @OA\Property(
     *          property="role_id",
     *          type="integer",
     *          example=5
     *      ),
     *      @OA\Property(
     *          property="role_name",
     *          type="string",
     *          example="Test Role"
     *      ),
     *      @OA\Property(
     *          property="initial_password",
     *          type="string",
     *          example="password123"
     *      )
     * )
     *
     * @var array
     */
    public $metadata;
}