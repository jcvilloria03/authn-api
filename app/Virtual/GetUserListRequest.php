<?php

namespace App\Virtual;

class GetUserListRequest
{
    /**
     * @OA\Parameter(
     *      name="email",
     *      in="query",
     *      description="User's email",
     *      required=false,
     *      @OA\Schema(type="string")
     * )
     *
     * @var string
     */
    public $email;

        /**
     * @OA\Parameter(
     *      name="name",
     *      in="query",
     *      description="User's name",
     *      required=false,
     *      @OA\Schema(type="string")
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Parameter(
     *      name="id",
     *      in="query",
     *      description="User's id",
     *      required=false,
     *      @OA\Schema(type="string")
     * )
     *
     * @var string
     */
    public $id;

    /**
     * @OA\Parameter(
     *      name="sort_by",
     *      in="query",
     *      description="Sort By",
     *      required=false,
     *      @OA\Schema(type="string")
     * )
     *
     * @var string
     */
    public $sortBy;

    /**
     * @OA\Parameter(
     *      name="sort_order",
     *      in="query",
     *      description="Sort Order",
     *      required=false,
     *      @OA\Schema(type="string")
     * )
     *
     * @var string
     */
    public $sortOrder;

    /**
     * @OA\Parameter(
     *      name="page",
     *      in="query",
     *      description="Page",
     *      required=false,
     *      @OA\Schema(type="string")
     * )
     *
     * @var string
     */
    public $page;

    /**
     * @OA\Parameter(
     *      name="per_page",
     *      in="query",
     *      description="Per Page",
     *      required=false,
     *      @OA\Schema(type="string")
     * )
     *
     * @var string
     */
    public $perPage;
}
