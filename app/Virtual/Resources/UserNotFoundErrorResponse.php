<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="UserNotFoundErrorResponse",
 *     description="Get user not found error response resource",
 *     @OA\Xml(
 *         name="UserNotFoundErrorResponse"
 *     )
 * )
 */
class UserNotFoundErrorResponse
{
    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     default="User Not Found",
     *     description="Error message"
     * )
     * @OA\Property(
     *     property="status",
     *     type="integer",
     *     default=404,
     *     description="Error status code"
     * )
     */
    public $message;
    public $status;
}