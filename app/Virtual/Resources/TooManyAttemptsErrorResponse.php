<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="TooManyAttemptsErrorResponse",
 *     description="Too Many attempts error sample response",
 *     @OA\Xml(
 *         name="TooManyAttemptsErrorResponse"
 *     )
 * )
 */
class TooManyAttemptsErrorResponse
{
    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     default="Too Many Attempts.",
     *     description="Error message"
     * )
     * @OA\Property(
     *     property="status",
     *     type="integer",
     *     default=429,
     *     description="Error status code"
     * )
     */
    public $message;
    public $status;
}
