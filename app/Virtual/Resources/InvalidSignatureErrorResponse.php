<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="InvalidSignatureErrorResponse",
 *     description="Invalid signature error sample response",
 *     @OA\Xml(
 *         name="InvalidSignatureErrorResponse"
 *     )
 * )
 */
class InvalidSignatureErrorResponse
{
    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     default="Invalid signature.",
     *     description="Error message"
     * )
     * @OA\Property(
     *     property="status",
     *     type="integer",
     *     default=403,
     *     description="Error status code"
     * )
     */
    public $message;
    public $status;
}
