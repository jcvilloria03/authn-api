<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="UnauthorizedErrorResponse",
 *     description="Unauthorized error sample response",
 *     @OA\Xml(
 *         name="UnauthorizedErrorResponse"
 *     )
 * )
 */
class UnauthorizedErrorResponse
{
    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     default="Unauthorized.",
     *     description="Error message"
     * )
     * @OA\Property(
     *     property="status",
     *     type="integer",
     *     default=401,
     *     description="Error status code"
     * )
     */
    public $message;
    public $status;
}
