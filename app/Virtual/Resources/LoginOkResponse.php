<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="Login Response",
 *     description="Example of successful login response",
 *     @OA\Xml(
 *         name="LoginOkResponse"
 *     )
 * )
 */
class LoginOkResponse
{
    /**
     * @OA\Property(
     *      title="data",
     *      description="Login response",
     *      type="object",
     *      @OA\Property(
     *          property="user",
     *          type="object",
     *          @OA\Property(
     *              property="id",
     *              type="int",
     *              example="113",
     *              description="Authn user ID"
     *          ),
     *          @OA\Property(
     *              property="user_id",
     *              type="int",
     *              example="60403",
     *              description="CP-API user ID"
     *          ),
     *          @OA\Property(
     *              property="employee_id",
     *              type="int",
     *              example="null",
     *              description="CP-API Employee ID"
     *          ),
     *          @OA\Property(
     *              property="first_name",
     *              type="string",
     *              example="Jonie",
     *              description="User's first name"
     *          ),
     *          @OA\Property(
     *              property="last_name",
     *              type="string",
     *              example="Dela Cruz",
     *              description="User's last name"
     *          ),
     *          @OA\Property(
     *              property="email",
     *              type="string",
     *              example="haralocal1700672144@authn.com",
     *              description="User's email"
     *          ),
     *          @OA\Property(
     *              property="account_id",
     *              type="int",
     *              example="8053",
     *              description="CP-API user's account ID"
     *          ),
     *          @OA\Property(
     *              property="account_name",
     *              type="string",
     *              example="Jonie Dela Cruz Company",
     *              description="User's account name"
     *          ),
     *          @OA\Property(
     *              property="company_id",
     *              type="int",
     *              example="12447",
     *              description="CP-API user's company ID"
     *          ),
     *          @OA\Property(
     *              property="company_name",
     *              type="string",
     *              example="Jonie Dela Cruz Company",
     *              description="User's company name"
     *          ),
     *          @OA\Property(
     *              property="role_id",
     *              type="int",
     *              example="7986",
     *              description="CP-API user's role ID"
     *          ),
     *          @OA\Property(
     *              property="role_name",
     *              type="string",
     *              example="Jonie Dela Cruz Company - Owner with ESS",
     *              description="User's role name"
     *          ),
     *          @OA\Property(
     *              property="user_type",
     *              type="string",
     *              example="owner",
     *              description="User type"
     *          ),
     *          @OA\Property(
     *              property="status",
     *              type="string",
     *              example="active",
     *              description="User's status"
     *          ),
     *          @OA\Property(
     *              property="last_login_at",
     *              type="string",
     *              example="2023-11-22T16:57:44.000000Z",
     *              description="User verified datetime"
     *          ),
     *          @OA\Property(
     *              property="email_verified_at",
     *              type="string",
     *              example="2023-11-22T16:57:44.000000Z",
     *              description="User verified datetime"
     *          ),
     *          @OA\Property(
     *              property="created_at",
     *              type="string",
     *              example="2023-11-22T16:57:44.000000Z",
     *              description="User's creation datetime"
     *          ),
     *          @OA\Property(
     *              property="updated_at",
     *              type="string",
     *              example="2023-11-23T02:38:45.000000Z",
     *              description="User's latest update datetime"
     *          ),
     *      ),
     *      @OA\Property(
     *          property="token",
     *          type="object",
     *          @OA\Property(
     *              property="access_token",
     *              type="string",
     *              example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI5YTljMDYwMy1mZTA2LTQ3YWYtYjk4YS03NjBiMzBjN2IzZjEiLCJqdGkiOiIzNWVmYzdjY2FkNDg0OTg4MjFhOTY2NDlmZjFhOTAyZDBkOTVmMThlM2E1YzVlYjUxYTBhYmViNDg3NDBlYTI2ZmFkMDZlY2MzZTM0MDEwOSIsImlhdCI6MTcwMDcwNzEyNC45MzM1MzgsIm5iZiI6MTcwMDcwNzEyNC45MzM1NCwiZXhwIjoxNzAwNzkzNTI0LjkyNjMyNSwic3ViIjoiMTEzIiwic2NvcGVzIjpbIioiXX0.pLpv3S-QeXxrDCPPXwyRtN1ByX4nUJ8wmQis0KNn2aaCOmv8cRcOcwmojYqN-tTJTFDzkzrO3azfPhvlH0PUsbzrOUmZjVGXGq6QvT1pBBTI8bm7TolZEvuEAnnOs5Hj1g7HJExWregiQrhvtCV6cH6RHIW4WkaAcPuz1544IAjmpxqmONQxCDmu43mF-sez6WLzSmr1K27dSX3qR6RqWATWnmXGAbMYJWXBeQymlpvEEGBzNn-6IKQvlvkPq4zI0lUGBer3qv-lIexcquQ8iByP5yNYvWqvKK2_15g5jxSupLStKHnjFVxLTiMXvXwrJWNguOYe18_llzkkIvue9Gcn7z1lEQdaTAQL2fi9wjGh_ZYwosyvhzLUyjborgS22sOQ3v3N8mgf3Xi0CAgWRXqByE_kDYr16YS9GvkpQXo1ZdwnexSZ8I0SQs-XkVRpMCu_7-2H_FkcSe1cNLo-ejSRSS9B9waKAotTwq02r5IWeGtRwAsv8HL7xUrppP-UQT2XGTEcUKkvOBrOfuFq7tnqK6IGR3BtA9ALzc6fm_rd7UTCobxCOLsgAgvZ1WCHRKZYru1WiZd1UlRDHK8lKMKlnSHO0jAVjIQqf7h3LJwev7PNNY__5jw4oMsG8bfrBJjwitUhMv3l-8q8sQQGJllfVbk4O05QgCt8LOsShgI"
     *          ),
     *          @OA\Property(
     *              property="expires_in",
     *              type="int",
     *              example="86400"
     *          )
     *      )
     * )
     *
     * @var array
     */
    public $data;
}
