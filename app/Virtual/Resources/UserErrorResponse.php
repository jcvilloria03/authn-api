<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="UserErrorResponse",
 *     description="User invalid error response resource",
 *     @OA\Xml(
 *         name="UserErrorResponse"
 *     )
 * )
 */
class UserErrorResponse
{
    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     default="The name field is required. (and 4 more errors)",
     *     description="Error message"
     * )
     * @OA\Property(
     *     property="errors",
     *     type="object",
     *     @OA\Property(
     *         property="name",
     *         type="array",
     *         @OA\Items(
     *             example="The name field is required."
     *         )
     *     ),
     *     @OA\Property(
     *         property="last_name",
     *         type="array",
     *         @OA\Items(
     *             example="The last name field is required."
     *         )
     *     ),
     *     @OA\Property(
     *         property="first_name",
     *         type="array",
     *         @OA\Items(
     *             example="The first name field is required."
     *         )
     *     ),
     *     @OA\Property(
     *         property="password",
     *         type="array",
     *         @OA\Items(
     *             example="The password field is required."
     *         )
     *     ),
     *     @OA\Property(
     *         property="email",
     *         type="array",
     *         @OA\Items(
     *             example="The email has already been taken."
     *         )
     *     ),
     *     @OA\Property(
     *         property="status",
     *         type="array",
     *         @OA\Items(
     *             example="The status field is required and should be one of 'new', 'pending', 'active', 'inactive'."
     *         )
     *     )
     * )
     * @OA\Property(
     *     property="status",
     *     type="integer",
     *     default=422,
     *     description="Error status code"
     * )
     */
    public $message;
    public $errors;
    public $status;
}