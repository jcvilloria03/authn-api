<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="GetUserErrorResponse",
 *     description="Get user invalid error response resource",
 *     @OA\Xml(
 *         name="GetUserErrorResponse"
 *     )
 * )
 */
class GetUserErrorResponse
{
    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     default="Path Not Found",
     *     description="Error message"
     * )
     * @OA\Property(
     *     property="status",
     *     type="integer",
     *     default=422,
     *     description="Error status code"
     * )
     */
    public $message;
    public $status;
}