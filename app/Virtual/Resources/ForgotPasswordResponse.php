<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="ForgotPasswordResponse",
 *     description="Get forgot password response resource",
 *     @OA\Xml(
 *         name="ForgotPasswordResponse"
 *     )
 * )
 */
class ForgotPasswordResponse
{
    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     default="Reset password link has been sent.",
     *     description="Success message"
     * )
     * @OA\Property(
     *     property="status",
     *     type="integer",
     *     default=200,
     *     description="Status code"
     * )
     */
    public $message;
    public $status;
}
