<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="ChangePasswordErrorResponse",
 *     description="Get email not found error response resource",
 *     @OA\Xml(
 *         name="ChangePasswordErrorResponse"
 *     )
 * )
 */
class ChangePasswordErrorResponse
{
    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     default="Passwords does not match.",
     *     description="Error message"
     * )
     * @OA\Property(
     *     property="status",
     *     type="integer",
     *     default=403,
     *     description="Error status code"
     * )
     */
    public $message;
    public $status;
}