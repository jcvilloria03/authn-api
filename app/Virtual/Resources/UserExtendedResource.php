<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="UserExtendedResource",
 *     description="User extended resource",
 *     @OA\Xml(
 *         name="UserExtendedResource"
 *     )
 * )
 */
class UserExtendedResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Virtual\Models\UserExtended[]
     */
    private $data;
}