<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="InternalServerErrorResponse",
 *     description="Internal server error sample response",
 *     @OA\Xml(
 *         name="InternalServerErrorResponse"
 *     )
 * )
 */
class InternalServerErrorResponse
{
    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     default="Unexpected error encountered. Please try again later.",
     *     description="Error message"
     * )
     * @OA\Property(
     *     property="status",
     *     type="integer",
     *     default=500,
     *     description="Error status code"
     * )
     */
    public $message;
    public $status;
}
