<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="EmailNotFoundErrorResponse",
 *     description="Get email not found error response resource",
 *     @OA\Xml(
 *         name="EmailNotFoundErrorResponse"
 *     )
 * )
 */
class EmailNotFoundErrorResponse
{
    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     default="Email Not Found.",
     *     description="Error message"
     * )
     * @OA\Property(
     *     property="status",
     *     type="integer",
     *     default=404,
     *     description="Error status code"
     * )
     */
    public $message;
    public $status;
}