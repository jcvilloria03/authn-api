<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="ChangePasswordResponse",
 *     description="Get change password response resource",
 *     @OA\Xml(
 *         name="ChangePasswordResponse"
 *     )
 * )
 */
class ChangePasswordResponse
{
    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     default="Change password successful.",
     *     description="Success message"
     * )
     * @OA\Property(
     *     property="status",
     *     type="integer",
     *     default=200,
     *     description="Status code"
     * )
     */
    public $message;
    public $status;
}