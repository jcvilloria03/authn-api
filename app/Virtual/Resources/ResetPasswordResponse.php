<?php

namespace App\Virtual\Resources;

/**
 * @OA\Schema(
 *     title="ResetPasswordResponse",
 *     description="Get reset password response resource",
 *     @OA\Xml(
 *         name="ResetPasswordResponse"
 *     )
 * )
 */
class ResetPasswordResponse
{
    /**
     * @OA\Property(
     *     property="message",
     *     type="string",
     *     default="Your password has been reset.",
     *     description="Success message"
     * )
     * @OA\Property(
     *     property="status",
     *     type="integer",
     *     default=200,
     *     description="Status code"
     * )
     */
    public $message;
    public $status;
}