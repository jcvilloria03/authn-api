<?php

namespace App\Virtual;

/**
 * @OA\Schema(
 *      title="Reset Password request",
 *      description="Reset Password request body data",
 *      type="object",
 *      required={"password","password_confirmation"}
 * )
 */

class ResetPasswordRequest
{
    /**
     * @OA\Property(
     *      title="Password",
     *      description="New password of the user",
     *      example="newpassword"
     * )
     *
     * @var string
     */
    public $password;

    /**
     * @OA\Property(
     *      title="Password Confirmation",
     *      description="Confirmation of the new password of the user",
     *      example="newpassword"
     * )
     *
     * @var string
     */
    public $password_confirmation;
}