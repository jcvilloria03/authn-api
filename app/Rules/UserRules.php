<?php

namespace App\Rules;

use App\Models\User;
use Illuminate\Validation\Rule;

class UserRules
{
    /**
     * Get the validation rules for creating a user.
     *
     * @return array The validation rules.
     */
    public static function getCreateRules()
    {
        return [
            'email' => 'required|email|unique:users',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'password' => 'required',
            'status' => [
                'required',
                Rule::in(User::STATUSES),
            ],
            'metadata' => 'required|array',
            'metadata.account_id' => 'sometimes|required|integer',
            'metadata.account_name' => 'sometimes|required|string',
            'metadata.company_id' => 'sometimes|integer',
            'metadata.company_name' => 'sometimes|string',
            'metadata.user_id' => 'sometimes|required|integer',
            'metadata.employee_id' => 'sometimes|integer',
            'metadata.role_id' => 'sometimes|required|integer',
            'metadata.role_name' => 'sometimes|required|string',
            'metadata.initial_password' => 'sometimes|required|string',
        ];
    }

    /**
     * Get the validation rules for updating a user.
     *
     * @return array The validation rules.
     */
    public static function getUpdateRules()
    {
        return [
            'email' => 'required|email|unique:users,email,' . request()->id,
            'first_name' => 'sometimes|required|max:255',
            'last_name' => 'sometimes|required|max:255',
            'status' => [
                'sometimes|required',
                Rule::in(User::STATUSES),
            ],
            'metadata' => 'sometimes|required|array',
            'metadata.account_id' => 'sometimes|required|integer',
            'metadata.account_name' => 'sometimes|required|string',
            'metadata.company_id' => 'sometimes|integer',
            'metadata.company_name' => 'sometimes|string',
            'metadata.user_id' => 'sometimes|required|integer',
            'metadata.employee_id' => 'sometimes|integer',
            'metadata.role_id' => 'sometimes|required|integer',
            'metadata.role_name' => 'sometimes|required|string',
            'metadata.initial_password' => 'sometimes|required|string',
        ];
    }

    /**
     * Get the validation rules for batch creating users.
     *
     * @return array The validation rules.
     */
    public static function getBatchCreateRules()
    {
        return [
            'email' => 'required|email|unique:users',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'password' => 'required',
            'status' => [
                'required',
                Rule::in(User::STATUSES),
            ],
            'account_id' => 'sometimes|required|integer',
            'account_name' => 'sometimes|required|string',
            'company_id' => 'sometimes|integer',
            'company_name' => 'sometimes|string',
            'user_id' => 'sometimes|required|integer',
            'employee_id' => 'sometimes|integer',
            'role_id' => 'sometimes|required|integer',
            'role_name' => 'sometimes|required|string',
            'initial_password' => 'sometimes|required|string',
        ];
    }

    /**
     * Get the validation rules for batch updating users.
     *
     * @return array The validation rules.
     */
    public static function getBatchUpdateRules()
    {
        return [
            'authn_user_id' => 'required|integer|exists:users,id',
            'email' => 'required|email',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'status' => [
                'required',
                Rule::in(User::STATUSES),
            ],
            'account_id' => 'sometimes|required|integer',
            'account_name' => 'sometimes|required|string',
            'company_id' => 'sometimes|integer',
            'company_name' => 'sometimes|string',
            'user_id' => 'sometimes|required|integer',
            'employee_id' => 'sometimes|integer',
            'role_id' => 'sometimes|required|integer',
            'role_name' => 'sometimes|required|string',
            'initial_password' => 'sometimes|required|string',
        ];
    }
}
