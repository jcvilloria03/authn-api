<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Services\UserAuthService;
use Illuminate\Auth\Events\Registered;

class CompanyUserConsumerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'consumer:authn_users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for Authn Users creation and update';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $queue = config('authn.consumers.authn_users');
        Amqp::consume(
            $queue,
            function ($message, $resolver) {
                try {
                    $this->process($message, $resolver);
                } catch (\Exception $e) {
                    Log::error($e->getMessage() . " : " . $message->body);
                    $resolver->reject($message);
                }
            },
            [
                'routing' => $queue
            ]
        );
    }

    /**
     * Function to run for each consumed message
     *
     * @param mixed $message
     * @param mixed $resolver
     * @return void
     */
    public function process($message, $resolver)
    {
        $details = json_decode(base64_decode($message->body), true);

        Log::info('UserCreateUpdateConsumer Received from ' . config('authn.consumers.authn_users'), [
            'request_data' => $details
        ]);
        if (empty($details)) {
            $resolver->acknowledge($message);
            return;
        }

        $userData = $details["data"];
        $messageType = $details["type"];

        if ($messageType == 'create') {
            $this->createUser($userData);
        } elseif ($messageType == 'update') {
            $this->updateUser($userData);
        } else {
            $this->info("Invalid message type: {$messageType}!");
        }

        $resolver->acknowledge($message);
    }

    /**
     * Function to create user
     *
     * @param mixed $userData
     * @return void
     */
    public function createUser($userData)
    {
        $this->info("Creating user with email: {$userData['email']}");

        try {
            $user = User::where('email', $userData['email'])->first();
            if ($user) {
                $this->info("User with email: {$userData['email']} already exists!");
                return;
            }

            $userData['metadata']['initial_password'] = $userData['password'];
            $userData['password'] = Hash::make($userData['password']);
            $userData['metadata']['for_change_password'] = 1;
            $userData['status'] = User::STATUS_NEW;

            $user = User::create($userData);

            event(new Registered($user));

            $this->dropMessage($user);
            $this->info("User with email: {$userData['email']} created!");
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $this->error("Error creating user with email: {$userData['email']}!");
        }

    }

    /**
     * Function to update user
     *
     * @param mixed $userData
     * @return void
     */
    public function updateUser($userData)
    {
        $this->info("Updating user with email: {$userData['email']}");

        try {
            $user = User::where('id', $userData['authn_user_id'])->first();
            $originalStatus = $user->status;
            if (!$user) {
                $this->info("User with email: {$userData['email']} does not exist!");
                return;
            }

            $isEmailUpdated = $user->email !== $userData['email'];

            // Remove password from update
            unset($userData['password']);

            if ($originalStatus == User::STATUS_NEW and $user->email_verified_at == null) {
                $userData['status'] = User::STATUS_NEW;
            }

            if ($isEmailUpdated) {
                $userData['email_verified_at'] = null;
                $user->update($userData);
                $user->sendEmailVerificationNotification();
            } else {
                $user->update($userData);
            }

            $this->dropMessage($user, 'update');
            $this->info("User with email: {$userData['email']} updated!");
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $this->error("Error updating user with email: {$userData['email']}!");
        }
    }

    // Drop a message to the queue
    protected function dropMessage($user, $messageType = 'create')
    {
        $userAuthService = app()->make(UserAuthService::class);
        $metadata = $user->metadata;
        $metadata['authn_user_id'] = $user->id;
        $metadata['is_update'] = $messageType == "update" ? true : false;

        $userAuthService->publishMessage(config('authn.queues.gateway_authn_user_create_update'),$metadata);

        Log::info('UserCreateUpdateConsumer Published to ' . config('authn.queues.gateway_authn_user_create_update'), [
            'user_id' => $user->id,
            'request_data' => $metadata
        ]);
    }
}