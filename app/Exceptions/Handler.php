<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Routing\Exceptions\InvalidSignatureException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (Throwable $e, Request $request) {
            if ($request->is('api/*')) {
                return $this->handleApiException($request, $e);
            }
        });
    }

    private function handleApiException($request, Throwable $exception)
    {
        $exception = $this->prepareException($exception);

        if ($exception instanceof \Illuminate\Http\Exception\HttpResponseException) {
            $exception = $exception->getResponse();
        }

        if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
            $exception = $this->unauthenticated($request, $exception);
        }

        if ($exception instanceof \Illuminate\Validation\ValidationException) {
            $exception = $this->convertValidationExceptionToResponse($exception, $request);
        }

        return $this->customApiResponse($exception);
    }

    private function customApiResponse($exception)
    {
        $statusCode = method_exists($exception, 'getStatusCode') && ! empty($exception->getStatusCode())
            ? $exception->getStatusCode()
            : (
                method_exists($exception, 'getCode') && ! empty($exception->getCode())
                ? $exception->getCode()
                : 500
            );

        $message = method_exists($exception, 'getMessage')
            ? $exception->getMessage()
            : null;

        $response = [];

        switch ($statusCode) {
            case 401:
                $response['message'] = $message ?? 'Unauthorized.';
                break;
            case 403:
                $response['message'] = $message ?? 'Forbidden.';
                break;
            case 404:
                $response['message'] = $message ?? 'Not Found.';
                break;
            case 406:
                $response['message'] = $message ?? 'Not Acceptable.';
                break;
            case 405:
                $response['message'] = $message ?? 'Method Not Allowed.';
                break;
            case 422:
                $response['message'] = $message ?? $exception->original['message'];
                break;
            default:
                $response['message'] = ($statusCode == 500)
                    ? 'Unexpected error encountered. Please try again later.'
                    : $message;
                break;
        }

        if (! empty($exception->original['errors'])) {
            $response['errors'] = $exception->original['errors'];
        }

        $response['status'] = $statusCode;

        if (config('app.debug') && method_exists($exception, 'getTrace')) {
            $response['trace'] = $exception->getTrace();
        }

        return response()->json($response, $statusCode);
    }

    public function render($request, Throwable $exception)
    {
        if ($exception instanceof InvalidSignatureException) {
            // If the exception is caused by an invalid or expired signed URL,
            // return the 'invalid-link' view
            return response()->view('errors.invalid-link', [], 403);
        }

        return $this->handleApiException($request, $exception);
    }
}
