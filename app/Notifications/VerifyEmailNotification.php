<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class VerifyEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * The verify email url.
     *
     * @var string
     */
    public $url;

    /**
     * The verify email template.
     *
     * @var string
     */
    public $template;

    /**
     * The user object.
     *
     * @var string
     */
    public $user;

    /**
     * Create a new notification instance.
     */
    public function __construct($url, $template, $user = null)
    {
        $this->url = $url;
        $this->template = $template;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('Verify your email address')
            ->markdown($this->template, ['url' => $this->url, 'user' => $this->user]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
