<?php

namespace App\Services;

use Illuminate\Support\Facades\Validator;
use League\Csv\Reader;
use App\Rules\UserRules;

class ValidationService
{
    /**
     * Validate a CSV file based on the given type.
     *
     * @param  mixed $file The CSV file to validate.
     * @param  string $type The type of validation to perform.
     * @return array The result of the validation.
     */
    public function validateCsv($file, $type)
    {
        $csv = Reader::createFromPath($file->getRealPath(), 'r');
        $csv->setHeaderOffset(0);

        if ($csv->count() === 0) {
            return $this->errorResponse('The CSV file is empty.');
        }

        $headers = $csv->getHeader();
        $rules = $this->getRulesBasedOnType($type);
        $flattenedRules = $this->flattenRules($rules);

        $requiredColumns = $this->getRequiredColumns($flattenedRules);
        $missingColumns = array_diff($requiredColumns, $headers);

        if (!empty($missingColumns)) {
            return $this->errorResponse('Missing columns: ' . implode(', ', $missingColumns));
        }

        $errors = $this->validateRecords($csv->getRecords(), $rules);

        if (!empty($errors)) {
            return $this->errorResponse('CSV file contains invalid data.', $errors);
        }

        return $this->successResponse('CSV file is valid.');
    }

    /**
     * Get the validation rules based on the given type.
     *
     * @param  string $type The type of validation to perform.
     * @return array The validation rules.
     */
    private function getRulesBasedOnType($type)
    {
        return $type === 'create' ? UserRules::getBatchCreateRules() : UserRules::getBatchUpdateRules();
    }

    /**
     * Flatten the validation rules.
     *
     * @param  array $rules The validation rules to flatten.
     * @return array The flattened validation rules.
     */
    private function flattenRules($rules)
    {
        return array_map(function ($rule) {
            return is_array($rule) ? implode('|', $rule) : $rule;
        }, $rules);
    }

    /**
     * Get the required columns from the flattened rules.
     *
     * @param  array $flattenedRules The flattened validation rules.
     * @return array The required columns.
     */
    private function getRequiredColumns($flattenedRules)
    {
        return array_keys(array_filter($flattenedRules, function ($rule) {
            return !str_contains($rule, 'sometimes');
        }));
    }

    /**
     * Validate the records in the CSV file.
     *
     * @param  array $records The records to validate.
     * @param  array $rules The validation rules.
     * @return array The validation errors.
     */
    private function validateRecords($records, $rules)
    {
        $errors = [];

        foreach ($records as $index => $record) {
            $recordRules = $rules;
            if (isset($record['authn_user_id'])) {
                $recordRules['email'] = 'required|email|unique:users,email,' . $record['authn_user_id'];
            }
            $validator = Validator::make($record, $recordRules);

            if ($validator->fails()) {
                $errors[$index] = $validator->errors()->toArray();
            }
        }

        return $errors;
    }

    /**
     * Create an error response.
     *
     * @param  string $message The error message.
     * @param  array|null $errors The validation errors.
     * @return array The error response.
     */
    private function errorResponse($message, $errors = null)
    {
        $response = ['status' => 'error', 'message' => $message];

        if ($errors !== null) {
            $response['errors'] = $errors;
        }

        return $response;
    }

    /**
     * Create a success response.
     *
     * @param  string $message The success message.
     * @return array The success response.
     */
    private function successResponse($message)
    {
        return ['status' => 'success', 'message' => $message];
    }
}
