<?php

namespace App\Services;

use App\Models\PasswordResetToken;
use Symfony\Component\HttpFoundation\Response;

class PasswordResetTokenService
{
    /**
     * Gets the Password Reset Token record using the email address
     *
     * @param string $email
     *
     * @return Collection $token
     */
    public function getPasswordResetToken(string $email)
    {
        $token = PasswordResetToken::where('email', $email)->first();

        return $token;
    }

    /**
     * Deletes the Password Reset Token record using the email address
     *
     * @param string $email
     *
     * @return int $token
     */
    public function deletePasswordResetToken(string $email)
    {
        $token = PasswordResetToken::where('email', $email)->delete();

        return $token;
    }
}
