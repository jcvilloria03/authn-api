<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Password;
use Laravel\Passport\Client as OauthClient;
use Laravel\Passport\RefreshTokenRepository;
use Laravel\Passport\TokenRepository;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Client\RequestException;
use Bschmitt\Amqp\Facades\Amqp;
use Bschmitt\Amqp\Message;

class UserAuthService
{
    /**
     * Creates user record
     *
     * @param  array  $data The user data.
     * @return App\Models\User The created user object
     */
    public function register($data)
    {
        $metadata = [
            'plan_id' => $data['plan_id'],
            'name' => $data['name'],
            'company' => $data['company'],
            'is_signup' => true,
        ];

        $user = new User();
        $user->email = $data['email'];
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->password = Hash::make($data['password']);
        $user->metadata = $metadata;
        $user->save();

        event(new Registered($user));

        return $user;
    }

    /**
     * Issues access and refresh tokens
     *
     * @param  \App\Models\User  $user The user object.
     * @param  array  $data The user credentials.
     * @return Illuminate\Http\Client\Response::json The created user access and refresh tokens
     */
    public function issueUserTokens(User $user, array $data)
    {
        $this->revokeUserExistingTokens($user);

        $client = OauthClient::where('id', env('OAUTH_CLIENT_ID'))->firstOrFail();

        $newTokens = Http::asForm()->post('http://authn-api/oauth/token', [
                'grant_type' => 'password',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'username' => $data['email'],
                'password' => $data['password'],
                'scope' => '*',
        ]);

        $user->update([
            'last_login_at' => now()->format('Y-m-d H:i:s'),
            'login_count' => $user->login_count + 1,
        ]);

        $data = [
            'access_token' => $newTokens['access_token'],
            'expires_in' => $newTokens['expires_in'],
        ];

        if (config('authn.enable_refresh_token')) {
            $data['refresh_token'] = $newTokens['refresh_token'];
        }

        return $data;
    }

    /**
     * Revokes access tokens of the given user
     *
     * @param  \App\Models\User  $user The user object.
     * @return void
     */
    public function revokeUserExistingTokens(User $user)
    {
        $existingAccessTokens = $user->tokens->where('revoked', 0);

        $tokenRepository = app(TokenRepository::class);
        $refreshTokenRepository = app(RefreshTokenRepository::class);
        foreach ($existingAccessTokens as $token) {
            $tokenRepository->revokeAccessToken($token->id);
            $refreshTokenRepository->revokeRefreshTokensByAccessTokenId($token->id);
        }

        $this->publishMessage(
            config('authn.queues.gateway_revoke_authn_tokens'),
            [
                'user_id' => $user->companyUserId()
            ]
        );
    }

    /**
     * Resets the user password
     *
     * @param  array  $param
     * @return $status
     */
    public function resetPassword($param)
    {
        $service = app()->make(UserService::class);
        $currentUser = $service->getUserByEmail($param['email']);

        $passwordTokenService = app()->make(PasswordResetTokenService::class);

        $token = $passwordTokenService->getPasswordResetToken($param['email']);

        abort_if(is_null($token->token), Response::HTTP_FORBIDDEN, 'Invalid token.');

        $status = Password::reset(
            $param,
            function (User $user, string $password) {
                $user->forceFill([
                    'password' => Hash::make($password),
                ]);

                $user->save();
            }
        );

        // Delete token
        $passwordTokenService->deletePasswordResetToken($token->email);

        $this->revokeUserExistingTokens($currentUser);

        return $status;
    }

    /**
     * Publish a message to a queue.
     *
     * @param string $queue
     * @param array $data
     * @return void
     */
    public function publishMessage(string $queue, array $data): void
    {
        Amqp::publish(
            $queue,
            new Message(
                base64_encode(json_encode($data)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1
                ]
            ),
            ['queue' => $queue]
        );
    }

    /**
     * Change user password
     *
     * @param  array  $data
     * @return $void
     */
    public function changePassword($data)
    {
        $service = app()->make(UserService::class);
        $user = $service->getUserByEmail($data['email']);

        $metadata = $user->metadata;
        unset($metadata['for_change_password']);

        $user['metadata'] = $metadata;
        $user['password'] = Hash::make($data['new_password']);
        $user->save();

        $this->revokeUserExistingTokens($user);
    }

    /**
     * Refresh access token
     *
     * @param  string  $refreshToken
     * @return $void
     */
    public function refreshToken($refreshToken)
    {
        $client = OauthClient::where('id', env('OAUTH_CLIENT_ID'))->firstOrFail();

        try {
            $newTokens = Http::asForm()->post('http://authn-api/oauth/token', [
                'grant_type' => 'refresh_token',
                'client_id' => $client->id,
                'client_secret' => $client->secret,
                'refresh_token' => $refreshToken,
                'scope' => '*',
            ])->throw();
        } catch (RequestException $e) {
            abort($e->response->getStatusCode(), $e->response->json()['message']);
        }

        return [
            'refresh_token' => $newTokens['refresh_token'],
            'access_token' => $newTokens['access_token'],
            'expires_in' => $newTokens['expires_in'],
        ];
    }

    /**
     * Email verification handling
     *
     * @param  \App\Models\User  $user The user object.
     * @return $void
     */
    public function verifyEmail(User $user)
    {
        $user->markEmailAsVerified();
        $user->status = User::STATUS_ACTIVE;
        $user->save();

        if ($user->isFromSignup()) {
            $companyService = app()->make(SalariumCompanyService::class);
            $companyService->createAccount($user);
        } else {
            $metadata = $user->metadata;
            $metadata['authn_user_id'] = $user->id;
            $metadata['verified_at'] = $user->verified_at;

            $this->publishMessage(config('authn.queues.gateway_authn_user_verified'),$metadata);
        }

        return true;
    }
}
