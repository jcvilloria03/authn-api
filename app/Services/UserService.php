<?php

namespace App\Services;

use App\Models\User;
use Bschmitt\Amqp\Facades\Amqp;
use Bschmitt\Amqp\Message;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserService
 * @package App\Services
 */
class UserService
{
    public function createUser($data)
    {
        // If metadata initial_password is empty, set it to equal password
        if (empty($data['metadata']['initial_password'])) {
            $data['metadata']['initial_password'] = $data['password'];
        }

        $data['status'] = User::STATUS_NEW;
        $data['password'] = Hash::make($data['password']);
        $data['created_by'] = auth()->user()->id;

        $data['metadata']['for_change_password'] = 1;

        $user = User::create($data);

        return $user;
    }

    /**
     * Returns the paginated user list
     *
     * @param  array  $filter The filter criteria for the user list
     * @return App\Models\User The paginated user object
     */
    public function getPaginatedUsers(array $filter = [])
    {
        $page = Arr::get($filter, 'page', 1);
        $perPage = Arr::get($filter, 'per_page', 10);

        return User::when(! empty($filter['email']), function ($q) use ($filter) {
            return $q->where('email', $filter['email']);
        })
            ->when(! empty($filter['id']), function ($q) use ($filter) {
                return $q->where('id', $filter['id']);
            })
            ->when(! empty($filter['name']), function ($q) use ($filter) {
                return $q->whereRaw("CASE WHEN ISNULL(middle_name) OR middle_name = ''
                THEN CONCAT(first_name, ' ', last_name) LIKE '%{$filter['name']}%'
                ELSE CONCAT(first_name, ' ', middle_name, ' ', last_name) LIKE '%{$filter['name']}%' END");
            })
            ->when(! empty($filter['sort_by']), function ($q) use ($filter) {
                $order = Arr::get($filter, 'sort_order', 'ASC');

                return $q->orderBy($filter['sort_by'], $order);
            })
            ->paginate($perPage, ['*'], 'page', $page);
    }

    /**
     * Returns the user data
     *
     * @param  int  $id The user id
     * @return App\Models\User The user object
     */
    public function getUserById(int $id)
    {
        $user = User::find($id);

        if (! $user) {
            abort(Response::HTTP_NOT_FOUND, 'User Not Found.');
        }

        return $user;
    }

    /**
     * Soft deletes the user data
     *
     * @param  int  $id The user id
     * @return int The copunt of deleted user
     */
    public function deleteUser(int $id)
    {
        $user = User::find($id);

        if (! $user) {
            abort(Response::HTTP_NOT_FOUND, 'User Not Found.');
        }

        return $user->delete();
    }

    public function updateUser($user, $data)
    {
        // Ensure we don't allow updating the password here
        unset($data['password']);
        $data['updated_by'] = auth()->user()->id;

        // Check if the email has been changed
        if ($user->email !== $data['email']) {
            // Set the email as unverified
            $data['email_verified_at'] = null;
        }

        $user->update($data);

        return $user;
    }

    /**
     * Returns the user data
     *
     * @param  string  $email The user's email
     * @return App\Models\User The user object
     */
    public function getUserByEmail(string $email)
    {
        $user = User::where('email', $email)->first();

        return $user;
    }

    /**
     * Updates user metadata with account data
     *
     * @param  App\Models\User  $user The user data.
     * @param  array  $data Data from cp-api account
     * @return void
     */
    public function saveAccountMetadata(User $user, array $data)
    {
        $metadata = $user->metadata;
        unset($metadata['name']);
        unset($metadata['company']);
        unset($metadata['is_signup']);

        $user->update([
            'metadata' => array_merge($metadata, $data),
            'status' => User::STATUS_ACTIVE,
        ]);

    }

    /**
     * Publish a message to a queue.
     *
     * @param string $queue
     * @param array $data
     * @return void
     */
    public function publishMessage(string $queue, array $data): void
    {
        Amqp::publish(
            $queue,
            new Message(
                base64_encode(json_encode($data)),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 1,
                ]
            ),
            ['queue' => $queue]
        );
    }
}
