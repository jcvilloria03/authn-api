<?php

namespace App\Services;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;

class SalariumCompanyService
{
    public const USER_TYPE_OWNER = 'owner';

    /**
     * Validates the given email in cp-api account and users table.
     *
     * @param string $email The sign up given email address.
     * @throws HttpException if the email is already taken.
     * @param bool
     */
    public function isEmailAvailable(string $email)
    {
        try {
            Http::asForm()
            ->post(
                config('authn.cp_api.base_uri') . '/account/email/is-available',
                ['email' => $email]
            )->throw();
        } catch (RequestException $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());

            abort($e->response->getStatusCode(), $e->response->json()['message']);
        }

        return true;
    }

    /**
     * Creates account in cp-api
     *
     * @param App\Models\User $user The user data.
     *
     * @throws HttpException if the account creation encounters error
     * @return void
     */
    public function createAccount(User $user)
    {
        // prepare the payload
        $payload = [
            'plan_id' => $user->metadata['plan_id'],
            'name' => $user->metadata['name'],
            'company' => $user->metadata['company'],
            'email' => $user->email,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'authn_id' => $user->id,
            'user_type' => self::USER_TYPE_OWNER,
        ];

        try {
            $response = Http::timeout(config('authn.cp_api.request_timeout'))
                ->asForm()
                ->post(
                    config('authn.cp_api.base_uri') . '/account/creation',
                    $payload
                )->throw();
        } catch (RequestException $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());

            abort($e->response->getStatusCode(), $e->response->json()['message']);
        }

        $accountData = $response->json();
        $userService = app()->make(UserService::class);
        $userService->saveAccountMetadata($user, $accountData);

        return;
    }
}
