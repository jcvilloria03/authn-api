<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserLoginResource;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\SalariumCompanyService;
use App\Services\UserAuthService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Symfony\Component\HttpFoundation\Response;

class UserAuthController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/v1/auth/signup",
     *     summary="Starting point of Salarium user account creation",
     *     tags={"Auth"},
     *
     *     @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/SignupUserRequest")
     *      ),
     *
     *      @OA\Response(
     *          response=202,
     *          description="Request has been accepted. Other account related data creation is now in progress.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/UserResource")
     *      ),
     *
     *     @OA\Response(
     *          response="422",
     *          description="Example of invalid arguments error.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/UserErrorResponse")
     *     ),
     *
     *     @OA\Response(
     *         response="429",
     *          description="Too Many Attempts.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/TooManyAttemptsErrorResponse")
     *     ),
     *
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     * )
     */
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'company' => 'required|max:255',
            'plan_id' => 'required|int|in:1,2,3',
            'password' => 'required',
        ]);

        // check in cp-api if email is available
        $companyService = app()->make(SalariumCompanyService::class);
        $companyService->isEmailAvailable($request->email);

        $data = $request->only([
            'email',
            'first_name',
            'last_name',
            'password',
            'name',
            'company',
            'plan_id',
        ]);

        $service = app()->make(UserAuthService::class);

        $user = $service->register($data);

        return (new UserResource($user))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/auth/email/verify/{id}/{hash}",
     *     summary="User email verification",
     *     tags={"Auth"},
     *
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="User ID",
     *         required=true,
     *
     *         @OA\Schema(type="integer")
     *     ),
     *
     *     @OA\Parameter(
     *         name="hash",
     *         in="path",
     *         description="Verification hash",
     *         required=true,
     *
     *         @OA\Schema(type="string")
     *     ),
     *
     *     @OA\Parameter(
     *         name="Signature",
     *         in="query",
     *         description="Verification signature",
     *         required=true,
     *
     *         @OA\Schema(type="string")
     *     ),
     *
     *      @OA\Response(
     *          response=302,
     *          description="Redirected.",
     *      ),
     *     @OA\Response(
     *          response="403",
     *          description="Invalid signature.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/InvalidSignatureErrorResponse")
     *     ),
     *
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     * )
     */
    public function verifyEmail(Request $request)
    {
        $user = User::findOrFail($request->route('id'));

        if ($user->hasVerifiedEmail()) {
            return redirect()->away(config('authn.pr_client.url.login'));
        }

        $service = app()->make(UserAuthService::class);

        $service->verifyEmail($user);

        return redirect()->away(config('authn.pr_client.url.welcome'));
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/email/verify/{id}/resend",
     *     summary="User email resend verification",
     *     tags={"Auth"},
     *
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="User ID",
     *         required=true,
     *
     *         @OA\Schema(type="integer")
     *     ),
     *
     *     @OA\Response(
     *          response="200",
     *          description="OK.",
     *
     *          @OA\JsonContent(
     *              type="object",
     *
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  default="Verification link sent!"
     *              )
     *          )
     *     ),
     *
     *     @OA\Response(
     *          response="422",
     *          description="Already verified.",
     *
     *          @OA\JsonContent(
     *              type="object",
     *
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  default="Email is already verified.",
     *                  description="Error message"
     *              ),
     *              @OA\Property(
     *                  property="status",
     *                  type="integer",
     *                  default="422",
     *                  description="Error status code"
     *              )
     *          )
     *     ),
     *
     *     @OA\Response(
     *         response="429",
     *          description="Too Many Attempts.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/TooManyAttemptsErrorResponse")
     *     ),
     *
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     * )
     */
    public function resendVerifyEmail(Request $request)
    {
        $user = User::findOrFail($request->route('id'));

        abort_if($user->hasVerifiedEmail(), Response::HTTP_UNPROCESSABLE_ENTITY, 'Email is already verified.');

        $user->sendEmailVerificationNotification();

        return response([
            'message' => 'Verification link sent!',
        ], Response::HTTP_OK);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/login",
     *     summary="User Login",
     *     description="Request for access token via password grant type.",
     *     tags={"Auth"},
     *
     *     @OA\RequestBody(
     *          description="Login credentials",
     *          required=true,
     *
     *          @OA\JsonContent(
     *              type="object",
     *
     *              @OA\Property(
     *                  property="email",
     *                  type="string",
     *                  default="haralocal1700672144@authn.com",
     *                  description="User's email"
     *              ),
     *              @OA\Property(
     *                  property="password",
     *                  type="string",
     *                  default="password@#1234",
     *                  description="User's password"
     *              )
     *          )
     *     ),
     *
     *     @OA\Response(
     *          response=200,
     *          description="OK",
     *
     *          @OA\JsonContent(ref="#/components/schemas/LoginOkResponse")
     *     ),
     *
     *     @OA\Response(
     *          response="422",
     *          description="Invalid parameters.",
     *
     *          @OA\JsonContent(
     *              type="object",
     *
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  default="The email field is required.",
     *                  description="Error message"
     *              ),
     *              @OA\Property(
     *                  property="errors",
     *                  type="object",
     *                  @OA\Property(
     *                      property="email",
     *                      type="array",
     *
     *                      @OA\Items(
     *                          example="The email field is required."
     *                      )
     *                  )
     *              ),
     *
     *              @OA\Property(
     *                  property="code",
     *                  type="integer",
     *                  default=422,
     *                  description="Error status code"
     *              )
     *          )
     *     ),
     *
     *     @OA\Response(
     *          response="201",
     *          description="User migration response.",
     *
     *          @OA\JsonContent(
     *              type="object",
     *
     *              @OA\Property(
     *                  property="for_reset_password",
     *                  type="integer",
     *                  default="1",
     *                  description="Flag for user migration"
     *              )
     *          )
     *     ),
     *
     *     @OA\Response(
     *          response="403",
     *          description="Invalid credentials.",
     *
     *          @OA\JsonContent(
     *              type="object",
     *
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  default="The provided credentials do not match our records.",
     *                  description="Error message"
     *              ),
     *              @OA\Property(
     *                  property="status",
     *                  type="integer",
     *                  default="403",
     *                  description="Error status code"
     *              )
     *          )
     *     ),
     *
     *     @OA\Response(
     *         response="429",
     *          description="Too Many Attempts.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/TooManyAttemptsErrorResponse")
     *     ),
     *
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     *  )
     *
     * Creates an authentication request using grant type password
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        $data = $request->only(['email', 'password']);
        $user = User::where('email', $request->email)->first();

        abort_if(empty($user), Response::HTTP_FORBIDDEN, 'The provided credentials do not match our records.');

        abort_if(
            !$user->hasVerifiedEmail(),
            Response::HTTP_FORBIDDEN,
            'Your account is for pending email verification. Please check your email address.'
        );

        abort_if(
            !$user->companyUserId(),
            Response::HTTP_UNPROCESSABLE_ENTITY,
            'Missing required metada. Unable to process request.'
        );

        // client migration notice
        if (empty($user->password)) {
            return response()->json(['data' => ['for_reset_password' => 1]]);
        }

        // authenticate credentials
        $authenticated = Auth::attempt($data);

        abort_if(empty($authenticated), Response::HTTP_FORBIDDEN, 'Invalid credentials.');

        $service = app()->make(UserAuthService::class);
        $tokens = $service->issueUserTokens(Auth::user(), $data);

        return (new UserLoginResource([
            'user' => $user->toArray(),
            'token' => $tokens,
        ]))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/forgot-password",
     *     summary="Sends a password reset email",
     *     tags={"Auth"},
     *
     *     @OA\Parameter(
     *         name="email",
     *         in="path",
     *         description="User's email",
     *         required=true,
     *
     *         @OA\Schema(type="string")
     *     ),
     *
     *     @OA\Response(
     *          response=200,
     *          description="OK",
     *
     *          @OA\JsonContent(ref="#/components/schemas/ForgotPasswordResponse")
     *     ),
     *
     *     @OA\Response(
     *          response="404",
     *          description="Email not found.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/EmailNotFoundErrorResponse")
     *     ),
     *
     *     @OA\Response(
     *         response="429",
     *          description="Too Many Attempts.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/TooManyAttemptsErrorResponse")
     *     ),
     *
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     * )
     */
    public function forgotPassword(Request $request)
    {
        $request->validate(['email' => 'required|email']);

        $service = app()->make(UserService::class);

        $user = $service->getUserByEmail($request->email);

        abort_if(is_null($user), Response::HTTP_NOT_FOUND, 'Email not found.');

        Password::sendResetLink(
            $request->only('email')
        );

        return response(
            [
                'message' => 'A password reset link has been sent to your email address. '
                . 'Please check your email address',
                'status' => Response::HTTP_OK,
            ],
            Response::HTTP_OK
        );
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/reset-password",
     *     summary="Resets user password",
     *     tags={"Auth"},
     *
     *     @OA\Parameter(
     *         name="email",
     *         in="path",
     *         description="User's email",
     *         required=true,
     *
     *         @OA\Schema(type="string")
     *     ),
     *
     *     @OA\Parameter(
     *         name="expires",
     *         in="path",
     *         description="Reset password expiration",
     *         required=true,
     *
     *         @OA\Schema(type="string")
     *     ),
     *
     *     @OA\Parameter(
     *         name="token",
     *         in="path",
     *         description="Reset password token",
     *         required=true,
     *
     *         @OA\Schema(type="string")
     *     ),
     *
     *     @OA\Parameter(
     *         name="signature",
     *         in="path",
     *         description="Reset password signature",
     *         required=true,
     *
     *         @OA\Schema(type="string")
     *     ),
     *
     *     @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/ResetPasswordRequest")
     *     ),
     *
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *
     *          @OA\JsonContent(ref="#/components/schemas/ResetPasswordResponse")
     *     ),
     *
     *     @OA\Response(
     *          response="403",
     *          description="Invalid signature.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/InvalidSignatureErrorResponse")
     *     ),
     *
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     * )
     */
    public function resetPassword(Request $request)
    {
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
        ]);

        $data = $request->only([
            'email',
            'password',
            'password_confirmation',
            'token',
        ]);

        $service = app()->make(UserAuthService::class);
        $status = $service->resetPassword($data);

        return $this->sendResetResponse($status);
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param  string  $status
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse($status)
    {
        switch ($status) {
            case Password::PASSWORD_RESET:
                return response()->json(
                    ['message' => __($status), 'status' => Response::HTTP_OK],
                    Response::HTTP_OK
                );
            case Password::INVALID_USER:
                return response()->json(
                    ['message' => __($status), 'status' => Response::HTTP_NOT_FOUND],
                    Response::HTTP_NOT_FOUND
                );
            case Password::INVALID_TOKEN:
                return response()->json(
                    ['message' => __($status), 'status' => Response::HTTP_BAD_REQUEST],
                    Response::HTTP_BAD_REQUEST
                );
            default:
                return response()->json(
                    ['message' => __('passwords.unknown_error'), 'status' => Response::HTTP_INTERNAL_SERVER_ERROR],
                    Response::HTTP_INTERNAL_SERVER_ERROR
                );
        }
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/change-password",
     *     summary="Change user password",
     *     tags={"Auth"},
     *     security={{"bearer_token":{}}},
     *
     *     @OA\RequestBody(
     *          required=true,
     *
     *          @OA\JsonContent(ref="#/components/schemas/ChangePasswordRequest")
     *     ),
     *
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *
     *          @OA\JsonContent(ref="#/components/schemas/ChangePasswordResponse")
     *     ),
     *
     *     @OA\Response(
     *          response=403,
     *          description="Pasword does not match.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/ChangePasswordErrorResponse")
     *     ),
     *
     *     @OA\Response(
     *          response=401,
     *          description="Unauthorized.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/UnauthorizedErrorResponse")
     *     ),
     *
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     * )
     */
    public function changePassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'old_password' => 'required|min:8',
            'new_password' => 'required|min:8',
            'password_confirmation' => 'required|min:8',
        ]);

        $data = $request->only([
            'email',
            'old_password',
            'new_password',
            'password_confirmation',
        ]);

        $service = app()->make(UserService::class);
        $user = $service->getUserByEmail($data['email']);

        abort_if(
            ! Hash::check($data['old_password'], $user['password']),
            Response::HTTP_FORBIDDEN,
            'Current password does not match.'
        );

        abort_if(
            $data['new_password'] !== $data['password_confirmation'],
            Response::HTTP_UNAUTHORIZED,
            'New password does not match.'
        );

        $service = app()->make(UserAuthService::class);
        $service->changePassword($data);

        return response(
            [
                'message' => 'Change password successful.',
                'status' => Response::HTTP_OK,
            ],
            Response::HTTP_OK
        );
    }

    /**
     * @OA\Get(
     *     path="/api/v1/auth/user",
     *     summary="Fetch user by access token",
     *     tags={"Auth"},
     *     security={{"bearer_token":{}}},
     *
     *     @OA\Response(
     *          response=200,
     *          description="OK",
     *
     *          @OA\JsonContent(
     *              type="object",
     *
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  default="Access token is valid."
     *              )
     *          )
     *     ),
     *
     *     @OA\Response(
     *          response=401,
     *          description="Unauthorized.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/UnauthorizedErrorResponse")
     *     ),
     *
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     * )
     */
    public function getUserByAccessToken(Request $request)
    {
        $request->validate([
            'client_id' => 'required',
            'client_secret' => 'required',
        ]);

        abort_if(
            !Auth::user()->hasVerifiedEmail(),
            Response::HTTP_UNAUTHORIZED,
            'User is not yet active.'
        );

        return (new UserLoginResource([
            'user' => Auth::user(),
        ]))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/logout",
     *     summary="Revoke user's existing access and refresh tokens.",
     *     tags={"Auth"},
     *     security={{"bearer_token":{}}},
     *
     *     @OA\Response(
     *          response=204,
     *          description="No Content."
     *     ),
     *
     *     @OA\Response(
     *          response=401,
     *          description="Unauthorized.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/UnauthorizedErrorResponse")
     *     ),
     *
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     * )
     */
    public function logout(Request $request)
    {
        $service = app()->make(UserAuthService::class);

        if (Auth::user()) {
            $service->revokeUserExistingTokens(Auth::user());
        }

        return response()->noContent();
    }

    /**
     * @OA\Post(
     *     path="/api/v1/auth/refresh_token",
     *     summary="Refresh user access and refresh tokens.",
     *     tags={"Auth"},
     *
     *     @OA\RequestBody(
     *          description="Refresh token",
     *          required=true,
     *
     *          @OA\JsonContent(
     *              type="object",
     *
     *              @OA\Property(
     *                  property="refresh_token",
     *                  type="string",
     *                  default="SampleRefreshToken",
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Fresh tokens.",
     *
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="access_token",
     *                  type="string",
     *                  example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI5YTljMDYwMy1mZTA2LTQ3YWYtYjk4YS03NjBiMzBjN2IzZjEiLCJqdGkiOiIzNWVmYzdjY2FkNDg0OTg4MjFhOTY2NDlmZjFhOTAyZDBkOTVmMThlM2E1YzVlYjUxYTBhYmViNDg3NDBlYTI2ZmFkMDZlY2MzZTM0MDEwOSIsImlhdCI6MTcwMDcwNzEyNC45MzM1MzgsIm5iZiI6MTcwMDcwNzEyNC45MzM1NCwiZXhwIjoxNzAwNzkzNTI0LjkyNjMyNSwic3ViIjoiMTEzIiwic2NvcGVzIjpbIioiXX0.pLpv3S-QeXxrDCPPXwyRtN1ByX4nUJ8wmQis0KNn2aaCOmv8cRcOcwmojYqN-tTJTFDzkzrO3azfPhvlH0PUsbzrOUmZjVGXGq6QvT1pBBTI8bm7TolZEvuEAnnOs5Hj1g7HJExWregiQrhvtCV6cH6RHIW4WkaAcPuz1544IAjmpxqmONQxCDmu43mF-sez6WLzSmr1K27dSX3qR6RqWATWnmXGAbMYJWXBeQymlpvEEGBzNn-6IKQvlvkPq4zI0lUGBer3qv-lIexcquQ8iByP5yNYvWqvKK2_15g5jxSupLStKHnjFVxLTiMXvXwrJWNguOYe18_llzkkIvue9Gcn7z1lEQdaTAQL2fi9wjGh_ZYwosyvhzLUyjborgS22sOQ3v3N8mgf3Xi0CAgWRXqByE_kDYr16YS9GvkpQXo1ZdwnexSZ8I0SQs-XkVRpMCu_7-2H_FkcSe1cNLo-ejSRSS9B9waKAotTwq02r5IWeGtRwAsv8HL7xUrppP-UQT2XGTEcUKkvOBrOfuFq7tnqK6IGR3BtA9ALzc6fm_rd7UTCobxCOLsgAgvZ1WCHRKZYru1WiZd1UlRDHK8lKMKlnSHO0jAVjIQqf7h3LJwev7PNNY__5jw4oMsG8bfrBJjwitUhMv3l-8q8sQQGJllfVbk4O05QgCt8LOsShgI"
     *              ),
     *              @OA\Property(
     *                  property="expires_in",
     *                  type="int",
     *                  example="86400"
     *              ),
     *              @OA\Property(
     *                  property="refresh_token",
     *                  type="string",
     *                  example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI5YTljMDYwMy1mZTA2LTQ3YWYtYjk4YS03NjBiMzBjN2IzZjEiLCJqdGkiOiIzNWVmYzdjY2FkNDg0OTg4MjFhOTY2NDlmZjFhOTAyZDBkOTVmMThlM2E1YzVlYjUxYTBhYmViNDg3NDBlYTI2ZmFkMDZlY2MzZTM0MDEwOSIsImlhdCI6MTcwMDcwNzEyNC45MzM1MzgsIm5iZiI6MTcwMDcwNzEyNC45MzM1NCwiZXhwIjoxNzAwNzkzNTI0LjkyNjMyNSwic3ViIjoiMTEzIiwic2NvcGVzIjpbIioiXX0.pLpv3S-QeXxrDCPPXwyRtN1ByX4nUJ8wmQis0KNn2aaCOmv8cRcOcwmojYqN-tTJTFDzkzrO3azfPhvlH0PUsbzrOUmZjVGXGq6QvT1pBBTI8bm7TolZEvuEAnnOs5Hj1g7HJExWregiQrhvtCV6cH6RHIW4WkaAcPuz1544IAjmpxqmONQxCDmu43mF-sez6WLzSmr1K27dSX3qR6RqWATWnmXGAbMYJWXBeQymlpvEEGBzNn-6IKQvlvkPq4zI0lUGBer3qv-lIexcquQ8iByP5yNYvWqvKK2_15g5jxSupLStKHnjFVxLTiMXvXwrJWNguOYe18_llzkkIvue9Gcn7z1lEQdaTAQL2fi9wjGh_ZYwosyvhzLUyjborgS22sOQ3v3N8mgf3Xi0CAgWRXqByE_kDYr16YS9GvkpQXo1ZdwnexSZ8I0SQs-XkVRpMCu_7-2H_FkcSe1cNLo-ejSRSS9B9waKAotTwq02r5IWeGtRwAsv8HL7xUrppP-UQT2XGTEcUKkvOBrOfuFq7tnqK6IGR3BtA9ALzc6fm_rd7UTCobxCOLsgAgvZ1WCHRKZYru1WiZd1UlRDHK8lKMKlnSHO0jAVjIQqf7h3LJwev7PNNY__5jw4oMsG8bfrBJjwitUhMv3l-8q8sQQGJllfVbk4O05QgCt8LOsShgI"
     *              )
     *          )
     *     ),
     *
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     * )
     */
    public function refreshToken(Request $request)
    {
        $request->validate([
            'refresh_token' => 'required'
        ]);

        $service = app()->make(UserAuthService::class);

        return $service->refreshToken($request->refresh_token);
    }
}
