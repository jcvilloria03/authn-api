<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;
use Illuminate\Auth\Events\Registered;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use App\Rules\UserRules;

class UserController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/v1/users",
     *     summary="Create user and send verification email",
     *     tags={"User"},
     *     security={{"bearer_token":{}}},
     *     @OA\RequestBody(
     *         description="User data",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/CreateUserRequest")
     *     ),
     *     @OA\Response(
     *          response=201,
     *          description="OK",
     *          @OA\JsonContent(ref="#/components/schemas/UserExtendedResource")
     *     ),
     *     @OA\Response(
     *          response="422",
     *          description="Example of invalid arguments error.",
     *          @OA\JsonContent(ref="#/components/schemas/UserErrorResponse")
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description="Unauthorized.",
     *          @OA\JsonContent(ref="#/components/schemas/UnauthorizedErrorResponse")
     *     ),
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     * )
     */
    public function createUser(Request $request)
    {
        $validator = Validator::make($request->all(), UserRules::getCreateRules());

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => $request->all(),
                'errors' => $validator->errors(),
            ], 422);
        }

        $attrbutes = $request->only([
            'email',
            'first_name',
            'last_name',
            'password',
            'middle_name',
            'mobile_number',
            'status',
            'metadata'
        ]);

        $service = app()->make(UserService::class);

        $user = $service->createUser($attrbutes);

        event(new Registered($user));

        return (new UserResource($user))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/users",
     *     summary="Fetch the user list",
     *     tags={"User"},
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *       ref="#/components/parameters/email"
     *     ),
     *     @OA\Parameter(
     *       ref="#/components/parameters/name"
     *     ),
     *     @OA\Parameter(
     *       ref="#/components/parameters/id"
     *     ),
     *     @OA\Parameter(
     *       ref="#/components/parameters/sort_by"
     *     ),
     *     @OA\Parameter(
     *       ref="#/components/parameters/sort_order"
     *     ),
     *     @OA\Parameter(
     *       ref="#/components/parameters/page"
     *     ),
     *     @OA\Parameter(
     *       ref="#/components/parameters/per_page"
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="OK",
     *          @OA\JsonContent(ref="#/components/schemas/UserResource")
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="User not found.",
     *          @OA\JsonContent(ref="#/components/schemas/UserNotFoundErrorResponse")
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description="Unauthorized.",
     *          @OA\JsonContent(ref="#/components/schemas/UnauthorizedErrorResponse")
     *     ),
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     * )
     */
    public function getUsers(Request $request)
    {
        $request->validate([
            'email' => 'sometimes|email',
            'name' => 'sometimes|max:255',
            'id' => 'sometimes|integer',
            'sort_by' => 'sometimes|in:first_name,email,id',
            'sort_order' => 'sometimes|string',
            'page' => 'sometimes|integer',
            'per_page' => 'sometimes|integer',
        ]);

        $service = app()->make(UserService::class);

        $filters = $request->only([
            'email',
            'name',
            'id',
            'sort_by',
            'sort_order',
            'page',
            'per_page'
        ]);

        $users = $service->getPaginatedUsers($filters);

        return new UserCollection($users);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/users/{id}",
     *     summary="Fetch a specific user by id",
     *     tags={"User"},
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="User's id",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="OK",
     *          @OA\JsonContent(ref="#/components/schemas/UserResource")
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="User not found.",
     *          @OA\JsonContent(ref="#/components/schemas/UserNotFoundErrorResponse")
     *     ),
     *     @OA\Response(
     *          response="422",
     *          description="Example of invalid arguments error.",
     *          @OA\JsonContent(ref="#/components/schemas/GetUserErrorResponse")
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description="Unauthorized.",
     *          @OA\JsonContent(ref="#/components/schemas/UnauthorizedErrorResponse")
     *     ),
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     * )
     */
    public function getUserById(int $id)
    {
        $service = app()->make(UserService::class);

        $user = $service->getUserById($id);

        return (new UserResource($user))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/users/{id}",
     *     summary="Delete a specific user by id",
     *     tags={"User"},
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="User's id",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response=204,
     *          description="No content",
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="User not found.",
     *          @OA\JsonContent(ref="#/components/schemas/UserNotFoundErrorResponse")
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description="Unauthorized.",
     *          @OA\JsonContent(ref="#/components/schemas/UnauthorizedErrorResponse")
     *     ),
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     * )
     */
    public function deleteUser(int $id)
    {
        $service = app()->make(UserService::class);

        $service->deleteUser($id);

        return response()->noContent();
    }

    /**
     * @OA\Put(
     *     path="/api/v1/users/{id}",
     *     summary="Update user",
     *     tags={"User"},
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the user to update",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="User data",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/UpdateUserRequest")
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="OK",
     *          @OA\JsonContent(ref="#/components/schemas/UserExtendedResource")
     *     ),
     *     @OA\Response(
     *          response="422",
     *          description="Example of invalid arguments error.",
     *          @OA\JsonContent(ref="#/components/schemas/UserErrorResponse")
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Internal Server Error",
     *     )
     * )
     */
    public function updateUser(Request $request, $id)
    {
        // Find the user by ID or fail with a 404 response
        $user = User::findOrFail($id);
        $originalEmail = $user->email;

        $validator = Validator::make($request->all(), UserRules::getUpdateRules(), [], ['id' => $id]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => $request->all(),
                'errors' => $validator->errors(),
            ], 422);
        }

        $attributes = $request->only([
            'email',
            'first_name',
            'last_name',
            'middle_name',
            'mobile_prefix',
            'mobile_number',
            'status',
            'metadata'
        ]);

        $service = app()->make(UserService::class);

        $updatedUser = $service->updateUser($user, $attributes);

        // Check if email was updated
        if ($updatedUser->email !== $originalEmail) {
            // Send verification email
            $updatedUser->sendEmailVerificationNotification();
        }

        return (new UserResource($user))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/users/via-email/{email}",
     *     summary="Fetch a specific user by id",
     *     tags={"User"},
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *         name="email",
     *         in="path",
     *         description="User's email",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="OK",
     *          @OA\JsonContent(ref="#/components/schemas/UserResource")
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="User not found.",
     *          @OA\JsonContent(ref="#/components/schemas/UserNotFoundErrorResponse")
     *     ),
     *     @OA\Response(
     *          response="422",
     *          description="Example of invalid arguments error.",
     *          @OA\JsonContent(ref="#/components/schemas/GetUserErrorResponse")
     *     ),
     *     @OA\Response(
     *          response=401,
     *          description="Unauthorized.",
     *          @OA\JsonContent(ref="#/components/schemas/UnauthorizedErrorResponse")
     *     ),
     *     @OA\Response(
     *         response="500",
     *          description="Internal server error.",
     *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
     *     )
     * )
     */
    public function getUserByEmail(Request $request)
    {
        validator($request->route()->parameters(), [
            'email' => 'required|email'
        ])->validate();

        $email = $request->route('email');

        $service = app()->make(UserService::class);

        $user = $service->getUserByEmail($email);

        if (is_null($user)) {
            return response()->json([
                'data' => [],
            ], 200);
        }

        return (new UserResource($user))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }
}
