<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Services\ValidationService;

/**
 * @OA\Post(
 *     path="/api/v1/users/batch/validation",
 *     summary="Validate a CSV file for batch user creation or update",
 *     tags={"Validation"},
 *     security={{"bearer_token":{}}},
 *     @OA\RequestBody(
 *         description="CSV file and type of operation",
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="csv",
 *                     description="CSV file",
 *                     type="string",
 *                     format="binary"
 *                 ),
 *                 @OA\Property(
 *                     property="type",
 *                     description="Type of operation ('create' or 'update')",
 *                     type="string"
 *                 ),
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="CSV file is valid",
 *         @OA\JsonContent(
 *             @OA\Property(property="status", type="string", example="success"),
 *             @OA\Property(property="message", type="string", example="CSV file is valid.")
 *         )
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="CSV file is invalid",
 *         @OA\JsonContent(
 *             @OA\Property(property="status", type="string", example="error"),
 *             @OA\Property(property="message", type="string",
 *                 example={
 *                     "CSV file is empty.",
 *                     "Missing columns: first_name, last_name",
 *                     "CSV file contains invalid data."
 *                 }
 *             ),
 *             @OA\Property(property="errors", type="object",
 *                 example={
 *                     "1": {
 *                         "email": "The email field must be a valid email address.",
 *                         "first_name": "The first name field is required.",
 *                         "last_name": "The last name field is required."
 *                     },
 *                     "2": {
 *                         "email": "The email has already been taken."
 *                     }
 *                 }
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response=401,
 *          description="Unauthorized.",
 *          @OA\JsonContent(ref="#/components/schemas/UnauthorizedErrorResponse")
 *     ),
 *     @OA\Response(
 *         response="500",
 *          description="Internal server error.",
 *          @OA\JsonContent(ref="#/components/schemas/InternalServerErrorResponse")
 *     )
 * )
 */
class ValidationController extends Controller
{
    public function validateCsv(Request $request)
    {
        $request->validate([
            'csv' => 'required|file|mimes:csv,txt',
            'type' => ['required', Rule::in(['create', 'update'])],
        ]);

        $validationService = app()->make(ValidationService::class);

        $result = $validationService->validateCsv($request->file('csv'), $request->type);

        $status = $result['status'] === 'success' ? 200 : 422;

        return response()->json($result, $status);
    }
}