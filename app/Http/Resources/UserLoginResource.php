<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserLoginResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [
            'user' => [
                'id' => $this->resource['user']['id'],
                'user_id' => optional($this->resource['user']['metadata'])['user_id'],
                'employee_id' => optional($this->resource['user']['metadata'])['employee_id'],
                'first_name' => $this->resource['user']['first_name'],
                'last_name' => $this->resource['user']['last_name'],
                'email' => $this->resource['user']['email'],
                'account_id' => optional($this->resource['user']['metadata'])['account_id'],
                'account_name' => optional($this->resource['user']['metadata'])['account_name'],
                'company_id' => optional($this->resource['user']['metadata'])['company_id'],
                'company_name' => optional($this->resource['user']['metadata'])['company_name'],
                'role_id' => optional($this->resource['user']['metadata'])['role_id'],
                'role_name' => optional($this->resource['user']['metadata'])['role_name'],
                'user_type' => optional($this->resource['user']['metadata'])['user_type'],
                'status' => $this->resource['user']['status'],
                'login_count' => $this->resource['user']['login_count'],
                'last_login_at' => $this->resource['user']['last_login_at'],
                'email_verified_at' => $this->resource['user']['email_verified_at'],
                'is_signup' => optional($this->resource['user']['metadata'])['is_signup'],
                'for_change_password' => optional($this->resource['user']['metadata'])['for_change_password'],
                'created_at' => $this->resource['user']['created_at'],
                'updated_at' => $this->resource['user']['updated_at'],
            ],
        ];

        if (! empty($this->resource['token'])) {
            $data['token'] = [
                'access_token' => $this->resource['token']['access_token'],
                'expires_in' => $this->resource['token']['expires_in'],
            ];

            if (optional($this->resource['token'])['refresh_token']) {
                $data['token']['refresh_token'] = $this->resource['token']['refresh_token'];
            }
        }

        return $data;
    }
}
