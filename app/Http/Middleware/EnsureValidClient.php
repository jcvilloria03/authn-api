<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Laravel\Passport\Client;
use Symfony\Component\HttpFoundation\Response;

class EnsureValidClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        abort_if(
            empty($request->client_id) || empty($request->client_secret),
            Response::HTTP_UNAUTHORIZED,
            'Missing client credential parameters.'
        );

        $client = Client::find($request->client_id);

        $revoked = $client?->revoked;
        $invalidSecret = $client?->secret !== $request->client_secret;

        abort_if(
            empty($client) || $invalidSecret || $revoked,
            Response::HTTP_UNAUTHORIZED,
            'Invalid client credentials.'
        );

        return $next($request);
    }
}
