# AuthN API

[![N|Solid](https://d1ondogjrfqdaz.cloudfront.net/assets/img/salarium_logos/salarium_logo_clear.png?v=efef369570dc6e1cce7e716bee28428b)](https://gitlab.salarium.com/salarium/code/authn-api)

# Introduction
AuthN is the Salarium API authentication system

## Tech Stack

* [Php 8.2] - Server-side scripting language commonly used for web development
* [Laravel] - Open-source PHP web application framework.
* [Laravel Passport] - Laravel Library that simplifies the process of adding API authentication to web applications.
* [Laravel Octane] - Laravel extension designed to improve the performance and scalability of web applications.
* [Swoole] - PHP extension that enables asynchronous and event-driven programming, enhancing the performance of web applications.
* [RabbitMQ] - Message broker software that helps applications communicate and exchange data in a flexible and reliable way
* [MySQL] - Open-source relational database management system (RDBMS) that stores and manages data for various applications.


## Installation and Running the app

### Installation
```bash
$ git clone https://gitlab.salarium.com/salarium/code/authn-api.git
$ cd authn-api
$ cp .env.example .env
$ php artisan key:generate
$ composer install
$ npm install
$ php artisan migrate
$ php artisan passport:install
$ php artisan l5-swagger:generate
```

### Running
```bash
$ php artisan octane:start --server=swoole --host=0.0.0.0 --port=8900 --watch
```

### WFH Setup

[WFH Repository](https://gitlab.salarium.com/salarium/code/wfh/-/tree/hara-local-ubuntu-22.04.03?ref_type=heads)


## License

This is not an opensource codebase, therefore not distributable. Any means of distributing this illegally may result in a legal law suit. You can view the private source [here](https://gitlab.salarium.com/development/authn-api) on Gitlab.

 - (c) Copyright 2023. Salarium
