<?php

use App\Http\Controllers\Auth\UserAuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ValidationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return app()->version();
});

Route::prefix('v1')->group(function () {
    Route::prefix('auth')->group(function () {
        Route::middleware('throttle:auth')->group(function () {
            Route::post('/signup', [UserAuthController::class, 'signup']);
            Route::post('/login', [UserAuthController::class, 'login']);

            Route::post('/forgot-password', [UserAuthController::class, 'forgotPassword'])
                ->name('password.request');
        });

        Route::get('/email/verify/{id}/{hash}', [UserAuthController::class, 'verifyEmail'])
            ->middleware(['throttle:auth', 'signed:relative'])
            ->where('id', '[0-9]+')
            ->name('verification.verify');

        Route::post('/reset-password', [UserAuthController::class, 'resetPassword'])
            ->middleware(['throttle:auth', 'signed:relative'])
            ->name('password.reset');

        Route::post('/change-password', [UserAuthController::class, 'changePassword'])
            ->middleware(['throttle:auth', 'auth:api']);

        Route::get('/user', [UserAuthController::class, 'getUserByAccessToken'])
            ->middleware(['auth:api', 'ensure-valid-client']);

        Route::post('/logout', [UserAuthController::class, 'logout'])
            ->middleware(['auth:api']);

        Route::post('/token/refresh', [UserAuthController::class, 'refreshToken']);

        Route::post('/email/verify/{id}/resend', [UserAuthController::class, 'resendVerifyEmail'])
            ->where('id', '[0-9]+')
            ->name('verification.send')
            ->middleware(['throttle:auth', 'auth:api', 'ensure-valid-client']);
    });

    Route::prefix('users')->group(function () {
        Route::middleware(['auth:api', 'ensure-valid-client'])->group(function () {
            Route::post('/', [UserController::class, 'createUser']);
            Route::get('/', [UserController::class, 'getUsers']);
            Route::get('/{id}', [UserController::class, 'getUserById'])->where('id', '[0-9]+');
            Route::delete('/{id}', [UserController::class, 'deleteUser'])->where('id', '[0-9]+');
            Route::put('/{id}', [UserController::class, 'updateUser'])->where('id', '[0-9]+');

            Route::post('/users/batch/validation', [ValidationController::class, 'validateCsv']);
            Route::get('/via-email/{email}', [UserController::class,'getUserByEmail']);
        });
    });
});
