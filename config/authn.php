<?php

return [
    'pr_client' => [
        'url' => [
            'login' => env('FRONTEND_LOGIN_URL', 'https://app-stg.eks.salarium.com/login'),
            'welcome' => env('FRONTEND_WELCOME_URL', 'https://app-stg.eks.salarium.com/welcome'),
            'reset_password' => env('FRONTEND_RESET_PASSWORD_URL', 'https://app-stg.eks.salarium.com/reset-password'),
        ],
    ],
    'oauth' => [
        'refresh_token' => [
            'ttl' => env('REFRESH_TOKEN_TTL', 10),
        ],
        'access_token' => [
            'ttl' => env('ACCESS_TOKEN_TTL', 1),
        ],
    ],
    'queues' => [
        'account_creation' => env('ACCOUNT_CREATION_QUEUE', 'authn_account_creation'),
        'company_authn_user_update' => env('COMPANY_AUTHN_USER_UPDATE_QUEUE', 'company_authn_user_update'),
        'gateway_authn_user_verified' => env('GATEWAY_AUTHN_USER_VERIFIED_QUEUE', 'gateway_authn_user_verified'),
        'gateway_revoke_authn_tokens' => env('GATEWAY_REVOKE_AUTHN_TOKENS_QUEUE', 'gateway_revoke_authn_tokens'),
        'gateway_authn_user_create_update' => env('GATEWAY_AUTHN_USER_CREATE_UPDATE_QUEUE', 'gateway_authn_user_create_update'),
    ],
    'consumers' => [
        'authn_users' => env('AUTHN_USERS_QUEUE', 'authn_users'),
    ],
    'cp_api' => [
        'base_uri' => env('COMPANY_API_URI', 'http://cp-api'),
        'request_timeout' => env('COMPANY_REQUEST_TIMEOUT', 120),
    ],
    'gw_api' => [
        'url' => env('GATEWAY_API_URL', 'https://gw-api-stg.eks.salarium.com')
    ],
    'rate_limit' => [
        'global' => env('RATE_LIMIT_PER_MINUTE_GLOBAL', 1000),
        'auth_email' => env('RATE_LIMIT_AUTH_PER_MINUTE_EMAIL', 10),
        'auth_id' => env('RATE_LIMIT_AUTH_PER_MINUTE_USER_ID', 5),
    ],
    'enable_refresh_token' => env('ENABLE_REFRESH_TOKEN', false),
    'verification' => [
        'expire' => env('VERIFICATION_EXPIRE_MINUTES', 7200),
    ],
];
