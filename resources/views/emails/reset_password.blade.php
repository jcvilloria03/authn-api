<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600');

        * {
            font-family: 'Source Sans Pro', sans-serif;
            color: #767676;
        }

        #brand {
            padding: 20px;
            text-align: left;
            z-index: 10;
        }

        #brand img {
            height: 60px;
        }

        #content {
            padding: 20px 20px;
            background: #f7f7f7;
            overflow-wrap: break-word;
        }

        #content > div {
            padding: 0 40px;
        }

        h2 {
            font-weight: 600;
        }

        p {
            margin: 40px 0;
        }

        p.notice {
            font-size: 13px;
            font-weight: 300;
        }

        #reset {
            text-decoration: none;
            background: #00A5E5;
            padding: 15px 25px;
            border-radius: 4px;
            color: #fff;
            font-weight: 600;
            display: inline-block;
            margin-bottom: 10px;
        }

        a {
            color: #00A5E5;
            text-decoration: none;
        }

        #copyright {
            padding: 20px;
            text-align: left;
            text-align: center;
        }
    </style>
</head>
<body>
    <center>
        <div style="width: 600px; overflow: hidden;">
            <div id="brand">
                <img src="https://app-a.salarium.com/assets/img/salarium_logos/salarium_logo_clear.png" alt="salarium" />
            </div>
            <div id="content">
                <div>
                    <h2 style="font-size: 19.5px; text-align:center;">Password Reset</h2>

                    <p style="font-size: 13px;text-align: center;">We received a request to reset your password to your Salarium account. To reset your password, please click the button below. If you did not request a password reset, please ignore this e-mail.</p>

                    <a id="reset" href="{{ $url }}">Reset Password</a>

                    <p style="font-size: 13px;text-align: center;">
                        If the above link does not work, copy and paste the following into the address bar of your browser<br /><br />
                        <span><a href="{{ $url }}">{{ $url }}</a></span>
                    </p>
                </div>
            </div>
            <div id="copyright">
                <sub>
                    © 2023 Salarium Solutions ALL RIGHTS RESERVED
                </sub>
            </div>
        </div>
    </center>
</body>
</html>