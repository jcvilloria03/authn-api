<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro');

        * {
            font-family: 'Source Sans Pro', sans-serif;
            color: #767676;
        }

        #brand img {
            height: 250px;
            z-index: 10;
        }

        #content {
            padding: 20px 20px;
            width: 600px;
            background: #f7f7f7;
            -webkit-box-shadow: inset 0px 0px 20px 10px rgba(0,0,0,0.1);
            -moz-box-shadow: inset 0px 0px 20px 10px rgba(0,0,0,0.1);
            box-shadow: inset 0px 0px 20px 10px rgba(0,0,0,0.1);
        }
        #content > div {
            padding: 0 40px;
        }
        #verify {
            text-decoration: none;
            background: #00A5E5;
            padding: 12px 15px;
            border-radius: 4px;
            color: #fff;
            font-weight: 600;
            display: inline-block;
        }

        span {
            color: #00A5E5;
        }

        #copyright {
            padding: 20px;
            text-align: left;
            box-shadow: 0px 0px 20px 9px rgba(0,0,0,0.1);
            text-align: center;
        }

        #greetings {
            text-align: left;
            margin: 0;
        }
    </style>
  </head>
  <body>
    <center>
        <div style="overflow: hidden;">
            <div id="content">
              @if(is_null($user['email_verified_at']) && $user['status'] === 'new')
                <img src="https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/SALARIUM-BLUE-LOGO.png" width="225px" alt="salarium-blue-logo" id="brand" />
                <br/>
                <img src="https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/welcome-email-employee.png" width="350px" alt="welcome-image-employee" />
                <br/>
                <table style="width: 400px;">
                  <tr>
                    <td>
                      <span style="font-size: 12px; color: #000000;">Use the log-in details below to verify your account:</span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                     <br/>
                     <strong><span style="font-size: 12px; color: #000000;">Registered Email Address:</span></strong>
                     <br/>
                     {{ $user['email'] }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <br/>
                      <strong><span style="font-size: 12px; color: #000000;">Temporary Password</span></strong>
                      <br/>
                      {{ $user['metadata']['initial_password'] }}
                    </td>
                  </tr>
                </table>

                <br/>
                <a href="{{ $url }}" id="verify">Verify Email Address</a>
                <br/>
                <br/>
                <br/>
                <span style="font-size: 13px; color: #00A4E4;"><a href="https://www.salarium.com/salarium-employee-self-service/">Watch Video Tutorial on How to Verify Email Address</a></span>
                <br/>
                <br/>

                <table style="width: 400px;">
                  <tr>
                    <td>
                      <img src="https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/Webinars-Icon.png" width="30px" height="30px" align="center" border="0" style="margin-left: 10px;" />
                    </td>
                    <td style="padding-left: 20px;">
                      <span style="font-size: 12px; color: #000000;">Learn more about Salarium Employee Self Service Portal</span>
                      <br/>
                      <span style="font-size: 12px; color: #00A4E4;">
                      <a href="https://www.salarium.com/salarium-employee-self-service/">Go to Salarium ESS Learning Site</a>
                      </span>
                    </td>
                  </tr>
                </table>

                <br/>
                <br/>
                <br/>
                <span style="font-size: 12px; color: #000000;">For inquiries and other concerns, you may reach out to our Customer Support Team by sending an email to <a id="email" href="mailto:support@salarium.com">support&#64;salarium.com</a>.</span>
              @else
                <img src="https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/SALARIUM-BLUE-LOGO.png" width="225px" alt="salarium-blue-logo" id="brand" />
                <br/>
                <br/>
                <strong><span style="font-size: 17px; color: #000000;">Welcome and thanks for choosing Salarium!</span></strong>
                <br/>
                <img src="https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/thank_you_and_welcome_page-02.png" width="350px" alt="salarium-welcome-image" />   
                <br/>
                <span style="font-size: 12px; color: #000000;">We’re excited to give you a hassle-free payroll experience that you deserve. But before anything else, verify your email address so we can start working on your next payroll.</span>   
                <br/>
                <br/>
                <a href="{{ $url }}" id="verify">Verify Email Address</a>
                <br/>
                <br/>
                

                <strong><span style="font-size: 14px; color: #00A4E4;">Need more help?</span></strong>
                <br/>
                <br/>
                <span style="font-size: 12px; color: #000000;">Our customer support team is here for you 24/7. If you’re feeling stuck or confused on what to do next, just let us know how we can help.</span>
                <br/>
                <br/>
                <strong><span style="font-size: 14px; color: #000000;">Customer Support</span></strong>
                <br/>
                <span style="font-size: 12px; color: #000000;">support@salarium.com</span>
              @endif
            </div>
            <div id="copyright">
                <sub>
                    © 2023 Salarium LTD. ALL RIGHTS RESERVED
                </sub>
            </div>
      </div>
    </center>
  </body>
</html>