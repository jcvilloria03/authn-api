<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro');

        * {
            font-family: 'Source Sans Pro', sans-serif;
            color: #767676;
        }

        #brand img {
            height: 250px;
            z-index: 10;
        }

        #content {
            padding: 20px 20px;
            width: 600px;
            background: #f7f7f7;
            -webkit-box-shadow: inset 0px 0px 20px 10px rgba(0,0,0,0.1);
            -moz-box-shadow: inset 0px 0px 20px 10px rgba(0,0,0,0.1);
            box-shadow: inset 0px 0px 20px 10px rgba(0,0,0,0.1);
        }
        #content > div {
            padding: 0 40px;
        }

        #verify {
            text-decoration: none;
            background: #00A5E5;
            padding: 12px 15px;
            border-radius: 4px;
            color: #fff;
            font-weight: 600;
            display: inline-block;
        }

        span {
            color: #00A5E5;
        }

        #copyright {
            padding: 20px;
            text-align: left;
            box-shadow: 0px 0px 20px 9px rgba(0,0,0,0.1);
            text-align: center;
        }

        #greetings {
            text-align: left;
            margin: 0;
        }
    </style>
  </head>
  <body>
    <center>
        <div style="overflow: hidden;">
            <div id="content">
                <img src="https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/SALARIUM-BLUE-LOGO.png" width="225px" alt="salarium-blue-logo" id="brand" />
                <br/>
                <br/>
                <strong><span style="font-size:17px;color:#000000">Welcome and thanks for choosing Salarium!</span></strong>
                <br/>
                <img src="https://frontendv3-assets-dev.s3.us-west-2.amazonaws.com/assets/thank_you_and_welcome_page-02.png" width="350px" alt="salarium-welcome-image" />
                <br/>
                <span style="font-size:12px;color:#000000">We’re excited to give you a hassle-free payroll experience that you deserve. But before anything else, verify your email address so we can start working on your next payroll.</span>
                <br/>
                <br/>
                <a href="{{ $url }}" id="verify">Verify Email Address</a>
                <br/>
                <br/>
                <strong><span style="font-size:14px;color:#00a4e4">Need more help?</span></strong>
                <br/>
                <br/>
                <span style="font-size:12px;color:#000000">Our customer support team is here for you 24/7. If you’re feeling stuck or confused on what to do next, just let us know how we can help.</span>
                <br/>
                <br/>
                <strong><span style="font-size:14px;color:#000000">Customer Support</span></strong>
                <br />
                <span style="font-size:12px;color:#000000"><a href="mailto:support@salarium.com" target="_blank">support@salarium.com</a></span>
              </div>
            <div id="copyright">
                <sub>
                    © 2023 Salarium LTD. ALL RIGHTS RESERVED
                </sub>
            </div>
      </div>
    </center>
  </body>
</html>