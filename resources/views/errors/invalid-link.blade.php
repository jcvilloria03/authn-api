<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invalid Link</title>
    <style>
        html, body {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            font-family: 'Roboto', sans-serif;
        }

        .card {
            width: 400px;
            height: 600px;
            margin: 0 auto;
            padding: 40px;
            border: 1px solid #ccc;
            border-radius: 5px;
            text-align: center;
            box-sizing: border-box;
            display: flex;
            flex-direction: column;
            justify-content: center;
            box-shadow: 0 12px 40px rgba(0,0,0,0.12);;
        }

        .circle {
            width: 100px;
            height: 100px;
            position: relative;
            margin: 0 auto;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 50%;
            background-color: white;
            border: 3px solid #d00e17;
        }

        .cross:before,
        .cross:after {
            content: "";
            position: absolute;
            width: 3px;
            height: 50px;
            background-color: #d00e17;
        }

        .cross:before {
            transform: rotate(45deg);
        }

        .cross:after {
            transform: rotate(-45deg);
        }

        .cross {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
            height: 100%;
        }

        h1 {
            margin: 0;
            margin-top: 20px;
            font-size: 30px;
        }
    </style>
</head>
<body>
    <div class="card">
        <div class="circle">
            <div class="cross"></div>
        </div>
        <h1>Error</h1>
        <p>Your email address could not be verified.</p>
    </div>
</body>
</html>